﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace rocadeclaracion
{
    public partial class frmLectorDNI : Form
    {
        frmPrincipal frmpadre;
        System.Globalization.CultureInfo cultureingles;
        InputLanguage lenguageoriginal, lenguageingles;

        public frmLectorDNI(frmPrincipal fpadre)
        {
            InitializeComponent();
            frmpadre = fpadre;
            this.ActiveControl = tbCodBarras;

            lenguageoriginal = InputLanguage.CurrentInputLanguage;

            cultureingles = System.Globalization.CultureInfo.GetCultureInfo("en-US");
            lenguageingles = InputLanguage.FromCulture(cultureingles);
            InputLanguage.CurrentInputLanguage = lenguageingles;


        }

        private void frmLectorDNI_FormClosing(object sender, FormClosingEventArgs e)
        {
            InputLanguage.CurrentInputLanguage = lenguageoriginal;
        }

        private void tbCodBarras_Leave(object sender, EventArgs e)
        {
            this.ActiveControl = tbCodBarras;
        }

        private void tbCodBarras_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                frmpadre.lecturacodigoDNI = tbCodBarras.Text;
                this.Close();
                this.Dispose();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }
    }
}
