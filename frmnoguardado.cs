﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace rocadeclaracion
{
    public partial class frmNoGuardado : Form
    {
        frmPrincipal frmcaller;

        public frmNoGuardado(frmPrincipal frmc)
        {
            frmcaller = frmc;
            InitializeComponent();

        }

        private void btnDescarta_Click(object sender, EventArgs e)
        {
            frmcaller.respuestanoguardado = "Descartar";
            this.Close();
        }

        private void btnNoDescarta_Click(object sender, EventArgs e)
        {
            frmcaller.respuestanoguardado = "No Descartar";
            this.Close();
        }
    }
}
