﻿namespace rocadeclaracion
{
    partial class frmOpciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.tbCarpetaPDFs = new System.Windows.Forms.TextBox();
            this.btnCarpetaPDFs = new System.Windows.Forms.Button();
            this.lblCarpetaPDFs = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.btnAplicar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbCarpetaPDFs
            // 
            this.tbCarpetaPDFs.Font = new System.Drawing.Font("Roboto", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCarpetaPDFs.Location = new System.Drawing.Point(21, 48);
            this.tbCarpetaPDFs.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbCarpetaPDFs.Name = "tbCarpetaPDFs";
            this.tbCarpetaPDFs.Size = new System.Drawing.Size(530, 26);
            this.tbCarpetaPDFs.TabIndex = 22;
            // 
            // btnCarpetaPDFs
            // 
            this.btnCarpetaPDFs.Location = new System.Drawing.Point(559, 48);
            this.btnCarpetaPDFs.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnCarpetaPDFs.Name = "btnCarpetaPDFs";
            this.btnCarpetaPDFs.Size = new System.Drawing.Size(39, 26);
            this.btnCarpetaPDFs.TabIndex = 23;
            this.btnCarpetaPDFs.Text = "...";
            this.btnCarpetaPDFs.UseVisualStyleBackColor = true;
            this.btnCarpetaPDFs.Click += new System.EventHandler(this.btnCarpetaPDFs_Click);
            // 
            // lblCarpetaPDFs
            // 
            this.lblCarpetaPDFs.AutoSize = true;
            this.lblCarpetaPDFs.BackColor = System.Drawing.Color.Gray;
            this.lblCarpetaPDFs.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCarpetaPDFs.Location = new System.Drawing.Point(16, 22);
            this.lblCarpetaPDFs.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCarpetaPDFs.Name = "lblCarpetaPDFs";
            this.lblCarpetaPDFs.Size = new System.Drawing.Size(242, 19);
            this.lblCarpetaPDFs.TabIndex = 24;
            this.lblCarpetaPDFs.Text = "Carpeta contenedora de los pdfs:";
            // 
            // btnCancelar
            // 
            this.btnCancelar.AutoSize = true;
            this.btnCancelar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            this.btnCancelar.FlatAppearance.BorderSize = 0;
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.ForeColor = System.Drawing.Color.White;
            this.btnCancelar.Location = new System.Drawing.Point(379, 129);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(113, 51);
            this.btnCancelar.TabIndex = 26;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnAceptar
            // 
            this.btnAceptar.AutoSize = true;
            this.btnAceptar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            this.btnAceptar.FlatAppearance.BorderSize = 0;
            this.btnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAceptar.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAceptar.ForeColor = System.Drawing.Color.White;
            this.btnAceptar.Location = new System.Drawing.Point(263, 129);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(105, 51);
            this.btnAceptar.TabIndex = 25;
            this.btnAceptar.Text = "&Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = false;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // btnAplicar
            // 
            this.btnAplicar.AutoSize = true;
            this.btnAplicar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            this.btnAplicar.FlatAppearance.BorderSize = 0;
            this.btnAplicar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAplicar.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAplicar.ForeColor = System.Drawing.Color.White;
            this.btnAplicar.Location = new System.Drawing.Point(498, 129);
            this.btnAplicar.Name = "btnAplicar";
            this.btnAplicar.Size = new System.Drawing.Size(100, 51);
            this.btnAplicar.TabIndex = 27;
            this.btnAplicar.Text = "A&plicar";
            this.btnAplicar.UseVisualStyleBackColor = false;
            this.btnAplicar.Click += new System.EventHandler(this.btnAplicar_Click);
            // 
            // frmOpciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::rocadeclaracion.Properties.Resources.fondoform_01;
            this.ClientSize = new System.Drawing.Size(621, 192);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.btnAplicar);
            this.Controls.Add(this.tbCarpetaPDFs);
            this.Controls.Add(this.btnCarpetaPDFs);
            this.Controls.Add(this.lblCarpetaPDFs);
            this.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmOpciones";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configuración";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.TextBox tbCarpetaPDFs;
        private System.Windows.Forms.Button btnCarpetaPDFs;
        private System.Windows.Forms.Label lblCarpetaPDFs;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.Button btnAplicar;
    }
}