﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace rocadeclaracion
{
    public partial class frmPrincipal : Form
    {
        bool guardado;
        frmNoGuardado fnoguard;
        frmOpciones fopc;
        public string respuestanoguardado;
        public string lecturacodigoDNI;

        public frmPrincipal()
        {
            InitializeComponent();
        }

        private void frmPrincipal_Load(object sender, EventArgs e)
        {
            Comun.CargaDeRegistro();
            this.MaximizedBounds = Screen.FromHandle(this.Handle).WorkingArea;
            this.WindowState = FormWindowState.Maximized;
            panelEntrevista.Left = (this.Width - panelEntrevista.Width) / 2;
            panelEntrevista.Height=this.Height-panelEntrevista.Top-50;
            btnLeerDNI.Left = (this.Width - btnLeerDNI.Width) / 2;
            nombretoolStripMenuItem.Text = "";

            guardado = true;
            nuevotoolStripMenuItem_Click(this, e);
        }

        private void tbApellido_TextChanged(object sender, EventArgs e)
        {
            guardado = false;
            nombretoolStripMenuItem.Text = tbNombre.Text + " " + tbApellido.Text;
        }

        private void nuevotoolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!guardado)  // Se guardó
            {
                fnoguard = new frmNoGuardado(this); // No, muestra mensaje
                fnoguard.ShowDialog(this);
                if (respuestanoguardado == "No Descartar")
                {
                    return;
                }
            }


            tbApellido.Text = "";
            tbNombre.Text = "";
            tbNacion.Text = "Argentina";
            cmbTipoDoc.SelectedIndex = 0;
            tbNoDoc.Text = "";
            dtpFechaEmision.Value = DateTime.Now;
            dtpFechaNac.Value = DateTime.Now;
            rbSexoFem.Checked= false;
            rbSexoMasc.Checked = false;
            rbSexoX.Checked = false;
            tbDirección.Text = "";
            tbOcupacion.Text = "";
            udPeso.Value = 0;
            udPeso.ResetText();
            udTalla.Value = 0;
            udTalla.ResetText();
            cmbGrupo.SelectedIndex = -1;
            cmbFactor.SelectedIndex = -1;

            foreach (Control c in panelEntrevista.Controls)
            {
                if (c is Panel)
                {
                    foreach (Control cc in c.Controls)
                    {
                        if (cc.Name.Substring(0, 4) == "rbNo")
                        {
                            ((RadioButton)cc).Checked = true;
                        }
                    }
                }
            }

            guardado = true;


        }

        private void exportarpdftoolStripMenuItem_Click(object sender, EventArgs e)
        {


            string filename = Comun.carpetapdfs + "\\" + tbNoDoc.Text + " - " + tbApellido.Text + ", " + tbNombre.Text + " - " +
                dtpFechaEmision.Value.Year.ToString("D4") + dtpFechaEmision.Value.Month.ToString("D2") + 
                dtpFechaEmision.Value.Day.ToString("D2") + ".pdf";


            Bullzip.PdfWriter.PdfSettings pdfsettings = new Bullzip.PdfWriter.PdfSettings();
            pdfsettings.PrinterName = "Bullzip PDF Printer";
            pdfsettings.SetValue("Output", filename);
            pdfsettings.SetValue("ShowPDF", "yes");
            pdfsettings.SetValue("ShowSettings", "never");
            pdfsettings.SetValue("ShowSaveAs", "always");
            pdfsettings.SetValue("ShowProgress", "always");
            pdfsettings.SetValue("ShowProgressFinished", "always");
            pdfsettings.SetValue("ConfirmOverwrite", "always");
            pdfsettings.SetValue("ColorModel", "GRAY");
            //pdfsettings.SetValue("SuppressErrors","yes");

            pdfsettings.WriteSettings(Bullzip.PdfWriter.PdfSettingsFileType.RunOnce);

            printDialog1.Document = printDocument1;
            printDialog1.PrinterSettings.PrinterName = "Bullzip PDF printer";
            printDocument1.Print();

        }

        private void Cambioendatos(object sender, EventArgs e)
        {
            guardado = false;
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            // Define grosor de las lineas
            Pen penlinea = new Pen(Color.Black, .4F); // Penlinea, ancho 1 se usa para bordes gruesos
            Pen penlineafina = new Pen(Color.Black, .1F); // penlineafina, ancho 0.1 se usa para dividir el título del resultado
            e.Graphics.PageUnit = GraphicsUnit.Millimeter; // unidades de la página en milímetros
            Graphics Canvas = e.Graphics;
            Canvas.PageUnit = GraphicsUnit.Millimeter;
            float dpiCorrectionX = Canvas.DpiX / 100; // para que las coordenadas estén siempre en milímetros
            float dpiCorrectionY = Canvas.DpiY / 100; // vertical también
            // defina Bounds como los márgenes de toda la página (en milímetros)
            Point[] Points = new Point[2];
            Points[0] = new Point(Convert.ToInt32(e.PageBounds.X * dpiCorrectionX), Convert.ToInt32(e.PageBounds.Y * dpiCorrectionY));
            Points[1] = new Point(Convert.ToInt32(e.PageBounds.Width * dpiCorrectionX), Convert.ToInt32(e.PageBounds.Height * dpiCorrectionY));
            Canvas.TransformPoints(System.Drawing.Drawing2D.CoordinateSpace.Page, System.Drawing.Drawing2D.CoordinateSpace.Device, Points);
            Rectangle Bounds = new Rectangle(Points[0].X, Points[0].Y, Points[1].X - 20, Points[1].Y - 20);

            // Define Letras
            Font fontmuni = new Font("Roboto", 18);
            Font fonttitulo = new Font("Roboto", 14, FontStyle.Bold);
            Font fonttabla = new Font("Roboto", 10, FontStyle.Bold);
            Font fontdato = new Font("Roboto", 10);
            Font fontsi = new Font("Roboto", 11, FontStyle.Bold);

            // guarda márgenes, ancho de página y alto de página
            float mizq = Bounds.Left+10;
            float msup = Bounds.Top+10;
            float anchopag = Bounds.Width;
            float altopag = Bounds.Height;

            // Dibuja marco como referencia
            // e.Graphics.DrawRectangle(penlineafina, mizq, msup, anchopag, altopag);


            // Imprime Escudo
            e.Graphics.DrawImage(pbEscudo.Image, (mizq + (anchopag - 17) / 2), msup+5, 17, 27);


            centrar(mizq, mizq+anchopag, msup + 37, "Municipalidad de General Roca", e, fontmuni);
            centrar(mizq, mizq + anchopag, msup + 45, "DECLARACION JURADA", e, fonttitulo);

            //Tabla para datos personales
//            e.Graphics.DrawRectangle(penlineafina, mizq, msup + 52, anchopag, 24); //Recuadro
            float anchocadena;
            e.Graphics.DrawString(lblApellido.Text, fonttabla, Brushes.Black, mizq + 1,msup + 53);
            anchocadena = e.Graphics.MeasureString(lblApellido.Text, fonttabla).Width;
            e.Graphics.DrawString(tbApellido.Text, fontdato, Brushes.Black, mizq + 1 + anchocadena, msup + 53);
            e.Graphics.DrawRectangle(penlineafina, mizq, msup + 52, 82, 6); // Apellido

            e.Graphics.DrawString(lblNombre.Text, fonttabla, Brushes.Black, mizq + 83, msup + 53);
            anchocadena = e.Graphics.MeasureString(lblNoDoc.Text, fonttabla).Width;
            e.Graphics.DrawString(tbNombre.Text, fontdato, Brushes.Black, mizq + 84+anchocadena, msup + 53);
            e.Graphics.DrawRectangle(penlineafina, mizq+82, msup + 52, 82, 6); // Nombre

            e.Graphics.DrawString(lblTitEdad.Text, fonttabla, Brushes.Black, mizq + 165, msup + 53);
            anchocadena = e.Graphics.MeasureString(lblTitEdad.Text, fonttabla).Width;
            e.Graphics.DrawString(lblEdad.Text, fontdato, Brushes.Black, mizq + 166+ anchocadena, msup + 53);
            e.Graphics.DrawRectangle(penlineafina, mizq + 164, msup+52,anchopag-164, 6); // Edad

            e.Graphics.DrawString(lblNacion.Text, fonttabla, Brushes.Black, mizq + 1, msup + 59);
            anchocadena = e.Graphics.MeasureString(lblNacion.Text, fonttabla).Width;
            e.Graphics.DrawString(tbNacion.Text, fontdato, Brushes.Black, mizq + 1 + anchocadena, msup + 59);
            e.Graphics.DrawRectangle(penlineafina, mizq, msup + 58, 82, 6); // Nacionalidad


            e.Graphics.DrawString(lblFechaNac.Text, fonttabla, Brushes.Black, mizq + 83, msup + 59);
            anchocadena = e.Graphics.MeasureString(lblFechaNac.Text, fonttabla).Width;
            e.Graphics.DrawString(dtpFechaNac.Value.ToShortDateString(), fontdato, Brushes.Black, mizq + 83 + anchocadena, msup + 59);
            e.Graphics.DrawRectangle(penlineafina, mizq + 82, msup + 58, 82, 6); // Fecha de Nacimiento

            e.Graphics.DrawString(lblSexo.Text, fonttabla, Brushes.Black, mizq + 165, msup + 59);
            anchocadena = e.Graphics.MeasureString(lblSexo.Text, fonttabla).Width;
            string sexo = "";
            if (rbSexoMasc.Checked) sexo = "M";
            if (rbSexoFem.Checked) sexo = "F";
            if (rbSexoX.Checked) sexo = "X";
            e.Graphics.DrawString(sexo, fontdato, Brushes.Black, mizq + 166 + anchocadena, msup + 59);
            e.Graphics.DrawRectangle(penlineafina, mizq + 164, msup + 58, anchopag - 164, 6); // Sexo

            e.Graphics.DrawString(cmbTipoDoc.Text+":", fonttabla, Brushes.Black, mizq + 1, msup + 65);
            anchocadena = e.Graphics.MeasureString(cmbTipoDoc.Text +":", fonttabla).Width;
            e.Graphics.DrawString(tbNoDoc.Text, fontdato, Brushes.Black, mizq + 1 +anchocadena, msup + 65);
            e.Graphics.DrawRectangle(penlineafina, mizq, msup + 64, 57, 6); // DNI

            e.Graphics.DrawString(lblDireccion.Text, fonttabla, Brushes.Black, mizq + 58, msup + 65);
            anchocadena = e.Graphics.MeasureString(lblDireccion.Text, fonttabla).Width;
            e.Graphics.DrawString(tbDirección.Text, fontdato, Brushes.Black, mizq + 59 + anchocadena, msup + 65);
            e.Graphics.DrawRectangle(penlineafina, mizq + 57, msup + 64, 87, 6); // Dirección


            e.Graphics.DrawString(lblOcupacion.Text , fonttabla, Brushes.Black, mizq + 145, msup + 65);
            anchocadena = e.Graphics.MeasureString(lblOcupacion.Text, fonttabla).Width;
            e.Graphics.DrawString(tbOcupacion.Text, fontdato, Brushes.Black, mizq + 145 + anchocadena, msup + 65);
            e.Graphics.DrawRectangle(penlineafina, mizq + 144, msup + 64, anchopag - 144, 6); // Ocupación

            e.Graphics.DrawString(lblPeso.Text, fonttabla, Brushes.Black, mizq + 1, msup + 71);
            anchocadena = e.Graphics.MeasureString(lblPeso.Text, fonttabla).Width;
            e.Graphics.DrawString(udPeso.Value.ToString() +" kg.", fontdato, Brushes.Black, mizq + 1 + anchocadena, msup + 71);
            e.Graphics.DrawRectangle(penlineafina, mizq, msup + 70, 57, 6); // Peso


            e.Graphics.DrawString(lblTalla.Text, fonttabla, Brushes.Black, mizq + 58, msup + 71);
            anchocadena = e.Graphics.MeasureString(lblTalla.Text, fonttabla).Width;
            e.Graphics.DrawString(udTalla.Value.ToString() + " cm.", fontdato, Brushes.Black, mizq + 58 + anchocadena, msup + 71);
            e.Graphics.DrawRectangle(penlineafina, mizq + 57, msup + 70, 39, 6); // Talla


            e.Graphics.DrawString("Grupo sanguíneo y factor Rh:", fonttabla, Brushes.Black, mizq + 97, msup + 71);
            anchocadena = e.Graphics.MeasureString("Grupo sanguíneo y factor Rh:", fonttabla).Width;
            e.Graphics.DrawString(cmbGrupo.Text+cmbFactor.Text, fontdato, Brushes.Black, mizq + 97 + anchocadena, msup + 71);
            e.Graphics.DrawRectangle(penlineafina, mizq + 96, msup + 70, anchopag - 96, 6); // Grupo Sanguíneo y factor Rh

            e.Graphics.DrawString("ENTREVISTA: Antecedentes generales y Médicos", fonttabla, Brushes.Black, mizq, msup + 80);
            
            // Tabla con respuestas
            float anchocolumna = anchopag / 2;

//            e.Graphics.DrawRectangle(penlineafina, mizq, msup + 86, anchocolumna, 84);
//            e.Graphics.DrawRectangle(penlineafina, mizq+anchocolumna, msup + 86, anchocolumna, 84);

            e.Graphics.DrawRectangle(penlineafina, mizq+anchocolumna-9, msup + 86, 9, 81);
            e.Graphics.DrawRectangle(penlineafina, mizq + anchopag - 9, msup + 86, 9, 75);


            // Preguntas 1 - 12
            e.Graphics.DrawString(lblPreg1.Text, fontdato, Brushes.Black, mizq + 1, msup + 87);
            if(rbSi1.Checked)
                e.Graphics.DrawString("SI",fontsi,Brushes.Black,mizq+anchocolumna-7,msup+87);
            else
                e.Graphics.DrawString("NO", fontdato, Brushes.Black, mizq+anchocolumna - 7, msup + 87);

            e.Graphics.DrawString(lblPreg12.Text, fontdato, Brushes.Black, mizq + anchocolumna + 1, msup + 87);
            if (rbSi12.Checked)
                e.Graphics.DrawString("SI", fontsi, Brushes.Black, mizq + anchopag - 7, msup + 87);
            else
                e.Graphics.DrawString("NO", fontdato, Brushes.Black, mizq + anchopag - 7, msup + 87);

            e.Graphics.DrawRectangle(penlineafina, mizq, msup + 86, anchopag, 6);

            // Preguntas 2 - 13
            e.Graphics.DrawString(lblPreg2.Text, fontdato, Brushes.Black, mizq + 1, msup + 98);
            if (rbSi2.Checked)
                e.Graphics.DrawString("SI", fontsi, Brushes.Black, mizq + anchocolumna - 7, msup + 98);
            else
                e.Graphics.DrawString("NO", fontdato, Brushes.Black, mizq + anchocolumna - 7, msup + 98);

            e.Graphics.DrawString(lblPreg13.Text.Substring(0,53), fontdato, Brushes.Black, mizq + anchocolumna + 1, msup + 93);
            e.Graphics.DrawString(lblPreg13.Text.Substring(53, 53), fontdato, Brushes.Black, mizq + anchocolumna + 1, msup + 98);
            e.Graphics.DrawString(lblPreg13.Text.Substring(106, 12), fontdato, Brushes.Black, mizq + anchocolumna + 1, msup + 103);
            if (rbSi13.Checked)
                e.Graphics.DrawString("SI", fontsi, Brushes.Black, mizq + anchopag - 7, msup + 98);
            else
                e.Graphics.DrawString("NO", fontdato, Brushes.Black, mizq + anchopag - 7, msup + 98);

            e.Graphics.DrawRectangle(penlineafina, mizq, msup + 92, anchopag, 16);

            // Preguntas 3 - 14
            e.Graphics.DrawString(lblPreg3.Text, fontdato, Brushes.Black, mizq + 1, msup + 109);
            if (rbSi3.Checked)
                e.Graphics.DrawString("SI", fontsi, Brushes.Black, mizq + anchocolumna - 7, msup + 109);
            else
                e.Graphics.DrawString("NO", fontdato, Brushes.Black, mizq + anchocolumna - 7, msup + 109);

            e.Graphics.DrawString(lblPreg14.Text, fontdato, Brushes.Black, mizq + anchocolumna + 1, msup + 109);
            if (rbSi14.Checked)
                e.Graphics.DrawString("SI", fontsi, Brushes.Black, mizq + anchopag - 7, msup + 109);
            else
                e.Graphics.DrawString("NO", fontdato, Brushes.Black, mizq + anchopag - 7, msup + 109);

            e.Graphics.DrawRectangle(penlineafina, mizq, msup + 108, anchopag, 6);

            // Preguntas 4 - 15
            e.Graphics.DrawString(lblPreg4.Text, fontdato, Brushes.Black, mizq + 1, msup + 115);
            if (rbSi4.Checked)
                e.Graphics.DrawString("SI", fontsi, Brushes.Black, mizq + anchocolumna - 7, msup + 115);
            else
                e.Graphics.DrawString("NO", fontdato, Brushes.Black, mizq + anchocolumna - 7, msup + 115);

            e.Graphics.DrawString(lblPreg15.Text, fontdato, Brushes.Black, mizq + anchocolumna + 1, msup + 115);
            if (rbSi15.Checked)
                e.Graphics.DrawString("SI", fontsi, Brushes.Black, mizq + anchopag - 7, msup + 115);
            else
                e.Graphics.DrawString("NO", fontdato, Brushes.Black, mizq + anchopag - 7, msup + 115);

            e.Graphics.DrawRectangle(penlineafina, mizq, msup + 114, anchopag, 6);

            // Preguntas 5 - 16
            e.Graphics.DrawString(lblPreg5.Text, fontdato, Brushes.Black, mizq + 1, msup + 121);
            if (rbSi5.Checked)
                e.Graphics.DrawString("SI", fontsi, Brushes.Black, mizq + anchocolumna - 7, msup + 121);
            else
                e.Graphics.DrawString("NO", fontdato, Brushes.Black, mizq + anchocolumna - 7, msup + 121);

            e.Graphics.DrawString(lblPreg16.Text, fontdato, Brushes.Black, mizq + anchocolumna + 1, msup + 121);
            if (rbSi16.Checked)
                e.Graphics.DrawString("SI", fontsi, Brushes.Black, mizq + anchopag - 7, msup + 121);
            else
                e.Graphics.DrawString("NO", fontdato, Brushes.Black, mizq + anchopag - 7, msup + 121);

            e.Graphics.DrawRectangle(penlineafina, mizq, msup + 120, anchopag, 6);

            // Preguntas 6 - 17
            e.Graphics.DrawString(lblPreg6.Text, fontdato, Brushes.Black, mizq + 1, msup + 127);
            if (rbSi6.Checked)
                e.Graphics.DrawString("SI", fontsi, Brushes.Black, mizq + anchocolumna - 7, msup + 127);
            else
                e.Graphics.DrawString("NO", fontdato, Brushes.Black, mizq + anchocolumna - 7, msup + 127);

            e.Graphics.DrawString(lblPreg17.Text, fontdato, Brushes.Black, mizq + anchocolumna + 1, msup + 127);
            if (rbSi17.Checked)
                e.Graphics.DrawString("SI", fontsi, Brushes.Black, mizq + anchopag - 7, msup + 127);
            else
                e.Graphics.DrawString("NO", fontdato, Brushes.Black, mizq + anchopag - 7, msup + 127);

            e.Graphics.DrawRectangle(penlineafina, mizq, msup + 126, anchopag, 6);

            // Preguntas 7 - 18
            e.Graphics.DrawString(lblPreg7.Text, fontdato, Brushes.Black, mizq + 1, msup + 133);
            if (rbSi7.Checked)
                e.Graphics.DrawString("SI", fontsi, Brushes.Black, mizq + anchocolumna - 7, msup + 133);
            else
                e.Graphics.DrawString("NO", fontdato, Brushes.Black, mizq + anchocolumna - 7, msup + 133);

            e.Graphics.DrawString(lblPreg18.Text, fontdato, Brushes.Black, mizq + anchocolumna + 1, msup + 133);
            if (rbSi18.Checked)
                e.Graphics.DrawString("SI", fontsi, Brushes.Black, mizq + anchopag - 7, msup + 133);
            else
                e.Graphics.DrawString("NO", fontdato, Brushes.Black, mizq + anchopag - 7, msup + 133);

            e.Graphics.DrawRectangle(penlineafina, mizq, msup + 132, anchopag, 6);

            // Preguntas 8 - 19
            e.Graphics.DrawString(lblPreg8.Text.Substring(0,44), fontdato, Brushes.Black, mizq + 1, msup + 139);
            e.Graphics.DrawString(lblPreg8.Text.Substring(45,11), fontdato, Brushes.Black, mizq + 1, msup + 144);
            if (rbSi8.Checked)
                e.Graphics.DrawString("SI", fontsi, Brushes.Black, mizq + anchocolumna - 7, msup + 141.5F);
            else
                e.Graphics.DrawString("NO", fontdato, Brushes.Black, mizq + anchocolumna - 7, msup + 141.5F);

            e.Graphics.DrawString(lblPreg19.Text, fontdato, Brushes.Black, mizq + anchocolumna + 1, msup + 141.5F);
            if (rbSi19.Checked)
                e.Graphics.DrawString("SI", fontsi, Brushes.Black, mizq + anchopag - 7, msup + 141.5F);
            else
                e.Graphics.DrawString("NO", fontdato, Brushes.Black, mizq + anchopag - 7, msup + 141.5F);

            e.Graphics.DrawRectangle(penlineafina, mizq, msup + 138, anchopag, 11);

            // Preguntas 9 - 20
            e.Graphics.DrawString(lblPreg9.Text, fontdato, Brushes.Black, mizq + 1, msup + 150);
            if (rbSi9.Checked)
                e.Graphics.DrawString("SI", fontsi, Brushes.Black, mizq + anchocolumna - 7, msup + 150);
            else
                e.Graphics.DrawString("NO", fontdato, Brushes.Black, mizq + anchocolumna - 7, msup + 150);

            e.Graphics.DrawString(lblPreg20.Text, fontdato, Brushes.Black, mizq + anchocolumna + 1, msup + 150);
            if (rbSi20.Checked)
                e.Graphics.DrawString("SI", fontsi, Brushes.Black, mizq + anchopag - 7, msup + 150);
            else
                e.Graphics.DrawString("NO", fontdato, Brushes.Black, mizq + anchopag - 7, msup + 150);

            e.Graphics.DrawRectangle(penlineafina, mizq, msup + 149, anchopag, 6);

            // Preguntas 10 - 21
            e.Graphics.DrawString(lblPreg10.Text, fontdato, Brushes.Black, mizq + 1, msup + 156);
            if (rbSi10.Checked)
                e.Graphics.DrawString("SI", fontsi, Brushes.Black, mizq + anchocolumna - 7, msup + 156);
            else
                e.Graphics.DrawString("NO", fontdato, Brushes.Black, mizq + anchocolumna - 7, msup + 156);

            e.Graphics.DrawString(lblPreg21.Text, fontdato, Brushes.Black, mizq + anchocolumna + 1, msup + 156);
            if (rbSi21.Checked)
                e.Graphics.DrawString("SI", fontsi, Brushes.Black, mizq + anchopag - 7, msup + 156);
            else
                e.Graphics.DrawString("NO", fontdato, Brushes.Black, mizq + anchopag - 7, msup + 156);

            e.Graphics.DrawRectangle(penlineafina, mizq, msup + 155, anchopag, 6);

            // Pregunta 11 
            e.Graphics.DrawString(lblPreg11.Text, fontdato, Brushes.Black, mizq + 1, msup + 162);
            if (rbSi11.Checked)
                e.Graphics.DrawString("SI", fontsi, Brushes.Black, mizq + anchocolumna - 7, msup + 162);
            else
                e.Graphics.DrawString("NO", fontdato, Brushes.Black, mizq + anchocolumna - 7, msup + 162);


            e.Graphics.DrawRectangle(penlineafina, mizq, msup + 161, anchocolumna, 6);

            // Firma y aclaración


            e.Graphics.DrawRectangle(penlineafina, mizq, msup + 171, anchopag, 30);

            e.Graphics.DrawString("Declaro que los datos aportados son correctos y completos, que no he omitido "
            + "información solicitada y conocer", fontdato, Brushes.Black, mizq + 1, msup + 172);

            e.Graphics.DrawString("que los mismos tienen carácter de Declaración Jurada.", fontdato, Brushes.Black, mizq + 1, msup + 176);

            e.Graphics.DrawString("Fecha: " + dtpFechaEmision.Value.ToShortDateString(), fontdato, Brushes.Black, mizq+1, msup + 196);

            e.Graphics.DrawLine(penlineafina,mizq+100,msup+195,mizq+170,msup+195);
            centrar(mizq + 100, mizq + 170, msup + 196, tbNombre.Text.ToUpper() + " " + tbApellido.Text.ToUpper(), e, fontdato);


            // Fin de página
            e.HasMorePages = false;


        }


        // Centra e imprime una cadena con los margenes y posición vertical indicada
        private void centrar(float izq, float der, float posy, string txt, System.Drawing.Printing.PrintPageEventArgs e, Font f)
        {
            float anchocadena = e.Graphics.MeasureString(txt, f).Width;
            float centrado = izq + (der - izq - anchocadena) / 2; // calcula la posición izquierda
            e.Graphics.DrawString(txt, f, Brushes.Black, centrado, posy); // Imprime la cadena en negro
        }

        private void dtpFechaNac_ValueChanged(object sender, EventArgs e)
        {
            guardado = false;
            int edad = dtpFechaEmision.Value.Year - dtpFechaNac.Value.Year; // resta los años
            if (dtpFechaEmision.Value.DayOfYear < dtpFechaNac.Value.DayOfYear - 1) edad--; // Si todavía no llego el día del cumpleaños resta 1.
            if (edad > 0) lblEdad.Text = edad.ToString();

        }

        // Permite solo ingreso de dígitos para el DNI
        private void tbNoDoc_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }

        private void frmPrincipal_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!guardado)  // Se guardó
            {
                fnoguard = new frmNoGuardado(this); // No, muestra mensaje
                fnoguard.ShowDialog(this);
                if (respuestanoguardado == "No Descartar")
                {
                    e.Cancel = true;
                    return;
                }
            }


        }

        private void imprimirtoolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Mostrar diálogo de impresión e imprimir
            printDialog1.Document = printDocument1;
            if (printDialog1.ShowDialog() == DialogResult.OK) printDocument1.Print();
        }

        private void toolStripMenuItemNuevoExam_Click(object sender, EventArgs e)
        {
            nuevotoolStripMenuItem_Click(this, e);
        }

        private void exportarPDFToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            exportarpdftoolStripMenuItem_Click(this, e);
        }

        private void toolStripMenuItemImprimir_Click(object sender, EventArgs e)
        {
            imprimirtoolStripMenuItem_Click(this, e);
        }

        private void toolStripMenuItemSalir_Click(object sender, EventArgs e)
        {
            salirToolStripMenuItem_Click(this, e);
        }

        private void configuraciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fopc = new frmOpciones();
            fopc.ShowDialog(this);
        }

        private void btnLeerDNI_Click(object sender, EventArgs e)
        {
            lecturacodigoDNI = "";
            frmLectorDNI flector = new frmLectorDNI(this);
            flector.ShowDialog(this);
            if (flector != null)
            {
                flector.Close();
                flector.Dispose();
            }
            if (lecturacodigoDNI != "")
            {
//                lecturacodigoDNI=lecturacodigoDNI.Replace('"','@');

                if (lecturacodigoDNI.Substring(0, 1) != "@") // Versión nueva?
                {
                    // Extracción de datos de cadena
                    string lecturatemp; // aca va guardando la cadena recortada
                    int empiezaapellido = lecturacodigoDNI.IndexOf("@"); // Primer arroba
                    lecturatemp = lecturacodigoDNI.Substring(empiezaapellido + 1); // Recorta el primer dato y la arroba
                    int empiezanombre = lecturatemp.IndexOf("@"); // Segunda arroba
                    tbApellido.Text = lecturatemp.Substring(0, empiezanombre); // Extrae el apellido
                    lecturatemp = lecturatemp.Substring(empiezanombre + 1); // Recorta el segundo dato (apellido) y la arroba
                    int empiezasexo = lecturatemp.IndexOf("@"); // Tercera arroba
                    tbNombre.Text = lecturatemp.Substring(0, empiezasexo); // Extrae nombre
                    lecturatemp = lecturatemp.Substring(empiezasexo + 1); // Recorta el tercer dato (nombre) y la arroba
                    int empiezaDNI = lecturatemp.IndexOf("@"); // Cuarta arroba
                    string letrasexo = lecturatemp.Substring(0, empiezaDNI); // Extrae sexo
                    if (letrasexo == "M")
                        rbSexoMasc.Checked = true;
                    if (letrasexo == "F")
                        rbSexoFem.Checked = true;
                    if (letrasexo == "X")
                        rbSexoX.Checked = true;
                    lecturatemp = lecturatemp.Substring(empiezaDNI + 1); // Recorta el cuarto dato (sexo) y la arroba
                    int empiezaejemplar = lecturatemp.IndexOf("@"); // Quinta arroba
                    tbNoDoc.Text = lecturatemp.Substring(0, empiezaejemplar); // Extrae DNI
                    lecturatemp = lecturatemp.Substring(empiezaejemplar + 1); // Recorta el quinto dato (DNI) y la arroba
                    int empiezafechanac = lecturatemp.IndexOf("@"); // Sexta arroba
                    lecturatemp = lecturatemp.Substring(empiezafechanac + 1); // Recorta el sexto dato (ejemplar) y la arroba
                    int empiezafechavenc = lecturatemp.IndexOf("@"); // Septima arroba
                    string strfechanac = lecturatemp.Substring(0, empiezafechavenc);

                    dtpFechaNac.Value = DateTime.ParseExact(strfechanac, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                }
                else // Versión vieja
                {
                    string lecturatemp; // aca va guardando la cadena recortada
                    lecturatemp = lecturacodigoDNI.Substring(1); // Recorta la primera arroba
                    int empiezadato2 = lecturatemp.IndexOf("@"); // Segunda arroba
                    tbNoDoc.Text = lecturatemp.Substring(0, empiezadato2).TrimEnd(' ');
                    lecturatemp = lecturatemp.Substring(empiezadato2 + 1); // Recorta el DNI y la segunda arroba
                    int empiezadato3 = lecturatemp.IndexOf("@"); // Tercera arroba
                    lecturatemp = lecturatemp.Substring(empiezadato3 + 1); // Recorta el dato 2 y la tercera arroba
                    int empiezaapellido = lecturatemp.IndexOf("@"); // cuarta arroba
                    lecturatemp = lecturatemp.Substring(empiezaapellido + 1); // Recorta el dato 3 y la cuarta arroba
                    int empiezanombre = lecturatemp.IndexOf("@"); // Quinta arroba
                    tbApellido.Text = lecturatemp.Substring(0, empiezanombre); // Extrae el apellido
                    lecturatemp = lecturatemp.Substring(empiezanombre + 1); // Recorta el cuarto dato (apellido) y la arroba
                    int empiezanacion = lecturatemp.IndexOf("@"); // Sexta arroba
                    tbNombre.Text = lecturatemp.Substring(0, empiezanacion); // Extrae nombre
                    lecturatemp = lecturatemp.Substring(empiezanacion + 1); // Recorta el quintodato (nombre) y la arroba
                    int empiezafechanac = lecturatemp.IndexOf("@"); // Septima arroba
                    lecturatemp = lecturatemp.Substring(empiezafechanac + 1); // Recorta el sexto dato (nacionalidad) y la arroba
                    int empiezasexo = lecturatemp.IndexOf("@"); // Octava arroba
                    string strfechanac = lecturatemp.Substring(0, empiezasexo);

                    dtpFechaNac.Value = DateTime.ParseExact(strfechanac, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    lecturatemp = lecturatemp.Substring(empiezasexo + 1); // Recorta el septimo dato (fecha de nacimiento) y la arroba
                    int empiezafechaem = lecturatemp.IndexOf("@"); // Novena arroba
                    string letrasexo = lecturatemp.Substring(0, empiezafechaem); // Extrae sexo
                    if (letrasexo == "M")
                        rbSexoMasc.Checked = true;
                    if (letrasexo == "F")
                        rbSexoFem.Checked = true;
                    if (letrasexo == "X")
                        rbSexoX.Checked = true;
                }
            }

        }



    }
}
