﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace rocadeclaracion
{
    public partial class frmOpciones : Form
    {
        public frmOpciones()
        {
            InitializeComponent();
            tbCarpetaPDFs.Text = rocadeclaracion.Properties.Settings.Default.carpetapdfs.ToString();


        }

        private void btnCarpetaPDFs_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = tbCarpetaPDFs.Text;
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                tbCarpetaPDFs.Text = folderBrowserDialog1.SelectedPath; // Guarda el path de la carpeta de PDFs en la textbox
            }
        }

        private void btnAplicar_Click(object sender, EventArgs e)
        {
            Comun.carpetapdfs = tbCarpetaPDFs.Text;
            rocadeclaracion.Properties.Settings.Default.carpetapdfs = tbCarpetaPDFs.Text;
            rocadeclaracion.Properties.Settings.Default.Save();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            btnAplicar_Click(this, e);
            this.Close();
        }
    }
}
