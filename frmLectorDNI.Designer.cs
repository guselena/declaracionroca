﻿namespace rocadeclaracion
{
    partial class frmLectorDNI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLectorDNI));
            this.pbCodBarras = new System.Windows.Forms.PictureBox();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.lblInstrucCodigo = new System.Windows.Forms.Label();
            this.tbCodBarras = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbCodBarras)).BeginInit();
            this.SuspendLayout();
            // 
            // pbCodBarras
            // 
            this.pbCodBarras.Image = ((System.Drawing.Image)(resources.GetObject("pbCodBarras.Image")));
            this.pbCodBarras.Location = new System.Drawing.Point(65, 13);
            this.pbCodBarras.Margin = new System.Windows.Forms.Padding(4);
            this.pbCodBarras.Name = "pbCodBarras";
            this.pbCodBarras.Size = new System.Drawing.Size(287, 217);
            this.pbCodBarras.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbCodBarras.TabIndex = 1;
            this.pbCodBarras.TabStop = false;
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            this.btnCancelar.FlatAppearance.BorderSize = 0;
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.ForeColor = System.Drawing.Color.White;
            this.btnCancelar.Location = new System.Drawing.Point(151, 258);
            this.btnCancelar.Margin = new System.Windows.Forms.Padding(5);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(113, 37);
            this.btnCancelar.TabIndex = 7;
            this.btnCancelar.Text = "CANCELAR";
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // lblInstrucCodigo
            // 
            this.lblInstrucCodigo.AutoSize = true;
            this.lblInstrucCodigo.BackColor = System.Drawing.Color.Transparent;
            this.lblInstrucCodigo.Location = new System.Drawing.Point(10, 234);
            this.lblInstrucCodigo.Name = "lblInstrucCodigo";
            this.lblInstrucCodigo.Size = new System.Drawing.Size(404, 19);
            this.lblInstrucCodigo.TabIndex = 6;
            this.lblInstrucCodigo.Text = "Por favor lea el código de barras del DNI del examinado.";
            // 
            // tbCodBarras
            // 
            this.tbCodBarras.Location = new System.Drawing.Point(163, 141);
            this.tbCodBarras.Name = "tbCodBarras";
            this.tbCodBarras.Size = new System.Drawing.Size(100, 27);
            this.tbCodBarras.TabIndex = 8;
            this.tbCodBarras.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbCodBarras_KeyDown);
            this.tbCodBarras.Leave += new System.EventHandler(this.tbCodBarras_Leave);
            // 
            // frmLectorDNI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::rocadeclaracion.Properties.Resources.fondoform_01;
            this.ClientSize = new System.Drawing.Size(426, 308);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.lblInstrucCodigo);
            this.Controls.Add(this.pbCodBarras);
            this.Controls.Add(this.tbCodBarras);
            this.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmLectorDNI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Lectura de DNI";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmLectorDNI_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pbCodBarras)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbCodBarras;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Label lblInstrucCodigo;
        private System.Windows.Forms.TextBox tbCodBarras;
    }
}