﻿namespace rocadeclaracion
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.msMenuPrincipal = new System.Windows.Forms.MenuStrip();
            this.principalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemNuevoExam = new System.Windows.Forms.ToolStripMenuItem();
            this.exportarPDFToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemImprimir = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemSalir = new System.Windows.Forms.ToolStripMenuItem();
            this.herramientasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configuraciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imprimirtoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportarpdftoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevotoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.separaciontoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nombretoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tituloVentanaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnLeerDNI = new System.Windows.Forms.Button();
            this.panelEntrevista = new System.Windows.Forms.Panel();
            this.panelPreg21 = new System.Windows.Forms.Panel();
            this.rbSi21 = new System.Windows.Forms.RadioButton();
            this.rbNo21 = new System.Windows.Forms.RadioButton();
            this.lblPreg21 = new System.Windows.Forms.Label();
            this.panelPreg20 = new System.Windows.Forms.Panel();
            this.rbSi20 = new System.Windows.Forms.RadioButton();
            this.rbNo20 = new System.Windows.Forms.RadioButton();
            this.lblPreg20 = new System.Windows.Forms.Label();
            this.panelPreg19 = new System.Windows.Forms.Panel();
            this.rbSi19 = new System.Windows.Forms.RadioButton();
            this.rbNo19 = new System.Windows.Forms.RadioButton();
            this.lblPreg19 = new System.Windows.Forms.Label();
            this.panelPreg18 = new System.Windows.Forms.Panel();
            this.rbSi18 = new System.Windows.Forms.RadioButton();
            this.rbNo18 = new System.Windows.Forms.RadioButton();
            this.lblPreg18 = new System.Windows.Forms.Label();
            this.panelPreg17 = new System.Windows.Forms.Panel();
            this.rbSi17 = new System.Windows.Forms.RadioButton();
            this.rbNo17 = new System.Windows.Forms.RadioButton();
            this.lblPreg17 = new System.Windows.Forms.Label();
            this.panelPreg16 = new System.Windows.Forms.Panel();
            this.rbSi16 = new System.Windows.Forms.RadioButton();
            this.rbNo16 = new System.Windows.Forms.RadioButton();
            this.lblPreg16 = new System.Windows.Forms.Label();
            this.panelPreg15 = new System.Windows.Forms.Panel();
            this.rbSi15 = new System.Windows.Forms.RadioButton();
            this.rbNo15 = new System.Windows.Forms.RadioButton();
            this.lblPreg15 = new System.Windows.Forms.Label();
            this.panelPreg14 = new System.Windows.Forms.Panel();
            this.rbSi14 = new System.Windows.Forms.RadioButton();
            this.rbNo14 = new System.Windows.Forms.RadioButton();
            this.lblPreg14 = new System.Windows.Forms.Label();
            this.panelPreg13 = new System.Windows.Forms.Panel();
            this.rbSi13 = new System.Windows.Forms.RadioButton();
            this.rbNo13 = new System.Windows.Forms.RadioButton();
            this.lblPreg13 = new System.Windows.Forms.Label();
            this.panelPreg12 = new System.Windows.Forms.Panel();
            this.rbSi12 = new System.Windows.Forms.RadioButton();
            this.rbNo12 = new System.Windows.Forms.RadioButton();
            this.lblPreg12 = new System.Windows.Forms.Label();
            this.tbNacion = new System.Windows.Forms.TextBox();
            this.lblNacion = new System.Windows.Forms.Label();
            this.panelPreg11 = new System.Windows.Forms.Panel();
            this.rbSi11 = new System.Windows.Forms.RadioButton();
            this.rbNo11 = new System.Windows.Forms.RadioButton();
            this.lblPreg11 = new System.Windows.Forms.Label();
            this.panelPreg10 = new System.Windows.Forms.Panel();
            this.rbSi10 = new System.Windows.Forms.RadioButton();
            this.rbNo10 = new System.Windows.Forms.RadioButton();
            this.lblPreg10 = new System.Windows.Forms.Label();
            this.panelPreg9 = new System.Windows.Forms.Panel();
            this.rbSi9 = new System.Windows.Forms.RadioButton();
            this.rbNo9 = new System.Windows.Forms.RadioButton();
            this.lblPreg9 = new System.Windows.Forms.Label();
            this.panelPreg8 = new System.Windows.Forms.Panel();
            this.rbSi8 = new System.Windows.Forms.RadioButton();
            this.rbNo8 = new System.Windows.Forms.RadioButton();
            this.lblPreg8 = new System.Windows.Forms.Label();
            this.panelPreg7 = new System.Windows.Forms.Panel();
            this.rbSi7 = new System.Windows.Forms.RadioButton();
            this.rbNo7 = new System.Windows.Forms.RadioButton();
            this.lblPreg7 = new System.Windows.Forms.Label();
            this.panelPreg6 = new System.Windows.Forms.Panel();
            this.rbSi6 = new System.Windows.Forms.RadioButton();
            this.rbNo6 = new System.Windows.Forms.RadioButton();
            this.lblPreg6 = new System.Windows.Forms.Label();
            this.panelPreg5 = new System.Windows.Forms.Panel();
            this.rbSi5 = new System.Windows.Forms.RadioButton();
            this.rbNo5 = new System.Windows.Forms.RadioButton();
            this.lblPreg5 = new System.Windows.Forms.Label();
            this.panelPreg4 = new System.Windows.Forms.Panel();
            this.rbSi4 = new System.Windows.Forms.RadioButton();
            this.rbNo4 = new System.Windows.Forms.RadioButton();
            this.lblPreg4 = new System.Windows.Forms.Label();
            this.panelPreg3 = new System.Windows.Forms.Panel();
            this.rbSi3 = new System.Windows.Forms.RadioButton();
            this.rbNo3 = new System.Windows.Forms.RadioButton();
            this.lblPreg3 = new System.Windows.Forms.Label();
            this.panelPreg2 = new System.Windows.Forms.Panel();
            this.rbSi2 = new System.Windows.Forms.RadioButton();
            this.rbNo2 = new System.Windows.Forms.RadioButton();
            this.lblPreg2 = new System.Windows.Forms.Label();
            this.panelPreg1 = new System.Windows.Forms.Panel();
            this.rbSi1 = new System.Windows.Forms.RadioButton();
            this.rbNo1 = new System.Windows.Forms.RadioButton();
            this.lblPreg1 = new System.Windows.Forms.Label();
            this.lblAntecedentes = new System.Windows.Forms.Label();
            this.cmbFactor = new System.Windows.Forms.ComboBox();
            this.lblFactor = new System.Windows.Forms.Label();
            this.cmbGrupo = new System.Windows.Forms.ComboBox();
            this.lblGrupo = new System.Windows.Forms.Label();
            this.lblCm = new System.Windows.Forms.Label();
            this.udTalla = new System.Windows.Forms.NumericUpDown();
            this.lblTalla = new System.Windows.Forms.Label();
            this.lblKg = new System.Windows.Forms.Label();
            this.udPeso = new System.Windows.Forms.NumericUpDown();
            this.lblPeso = new System.Windows.Forms.Label();
            this.tbOcupacion = new System.Windows.Forms.TextBox();
            this.lblOcupacion = new System.Windows.Forms.Label();
            this.tbDirección = new System.Windows.Forms.TextBox();
            this.lblDireccion = new System.Windows.Forms.Label();
            this.rbSexoX = new System.Windows.Forms.RadioButton();
            this.rbSexoFem = new System.Windows.Forms.RadioButton();
            this.lblEdad = new System.Windows.Forms.Label();
            this.rbSexoMasc = new System.Windows.Forms.RadioButton();
            this.dtpFechaNac = new System.Windows.Forms.DateTimePicker();
            this.dtpFechaEmision = new System.Windows.Forms.DateTimePicker();
            this.lblSexo = new System.Windows.Forms.Label();
            this.lblFechaNac = new System.Windows.Forms.Label();
            this.lblFechaEmision = new System.Windows.Forms.Label();
            this.tbNoDoc = new System.Windows.Forms.TextBox();
            this.cmbTipoDoc = new System.Windows.Forms.ComboBox();
            this.lblNoDoc = new System.Windows.Forms.Label();
            this.lblTipoDoc = new System.Windows.Forms.Label();
            this.tbNombre = new System.Windows.Forms.TextBox();
            this.lblNombre = new System.Windows.Forms.Label();
            this.tbApellido = new System.Windows.Forms.TextBox();
            this.lblApellido = new System.Windows.Forms.Label();
            this.lblTitEdad = new System.Windows.Forms.Label();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.pbEscudo = new System.Windows.Forms.PictureBox();
            this.msMenuPrincipal.SuspendLayout();
            this.panelEntrevista.SuspendLayout();
            this.panelPreg21.SuspendLayout();
            this.panelPreg20.SuspendLayout();
            this.panelPreg19.SuspendLayout();
            this.panelPreg18.SuspendLayout();
            this.panelPreg17.SuspendLayout();
            this.panelPreg16.SuspendLayout();
            this.panelPreg15.SuspendLayout();
            this.panelPreg14.SuspendLayout();
            this.panelPreg13.SuspendLayout();
            this.panelPreg12.SuspendLayout();
            this.panelPreg11.SuspendLayout();
            this.panelPreg10.SuspendLayout();
            this.panelPreg9.SuspendLayout();
            this.panelPreg8.SuspendLayout();
            this.panelPreg7.SuspendLayout();
            this.panelPreg6.SuspendLayout();
            this.panelPreg5.SuspendLayout();
            this.panelPreg4.SuspendLayout();
            this.panelPreg3.SuspendLayout();
            this.panelPreg2.SuspendLayout();
            this.panelPreg1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udTalla)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udPeso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEscudo)).BeginInit();
            this.SuspendLayout();
            // 
            // msMenuPrincipal
            // 
            this.msMenuPrincipal.BackColor = System.Drawing.Color.Black;
            this.msMenuPrincipal.Font = new System.Drawing.Font("Roboto", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.msMenuPrincipal.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.msMenuPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.principalToolStripMenuItem,
            this.salirToolStripMenuItem,
            this.imprimirtoolStripMenuItem,
            this.exportarpdftoolStripMenuItem,
            this.nuevotoolStripMenuItem,
            this.separaciontoolStripMenuItem,
            this.nombretoolStripMenuItem,
            this.tituloVentanaToolStripMenuItem});
            this.msMenuPrincipal.Location = new System.Drawing.Point(0, 0);
            this.msMenuPrincipal.Name = "msMenuPrincipal";
            this.msMenuPrincipal.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.msMenuPrincipal.ShowItemToolTips = true;
            this.msMenuPrincipal.Size = new System.Drawing.Size(1354, 40);
            this.msMenuPrincipal.TabIndex = 1;
            this.msMenuPrincipal.Text = "menuStrip1";
            // 
            // principalToolStripMenuItem
            // 
            this.principalToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.principalToolStripMenuItem.AutoToolTip = true;
            this.principalToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.principalToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem,
            this.herramientasToolStripMenuItem});
            this.principalToolStripMenuItem.Font = new System.Drawing.Font("Roboto", 12F);
            this.principalToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.principalToolStripMenuItem.Image = global::rocadeclaracion.Properties.Resources.despliegah;
            this.principalToolStripMenuItem.Name = "principalToolStripMenuItem";
            this.principalToolStripMenuItem.Size = new System.Drawing.Size(44, 36);
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(50)))), ((int)(((byte)(73)))));
            this.archivoToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.archivoToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemNuevoExam,
            this.exportarPDFToolStripMenuItem1,
            this.toolStripMenuItemImprimir,
            this.toolStripMenuItemSalir});
            this.archivoToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(173, 24);
            this.archivoToolStripMenuItem.Text = "&Archivo";
            this.archivoToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripMenuItemNuevoExam
            // 
            this.toolStripMenuItemNuevoExam.AutoSize = false;
            this.toolStripMenuItemNuevoExam.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(50)))), ((int)(((byte)(73)))));
            this.toolStripMenuItemNuevoExam.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripMenuItemNuevoExam.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemNuevoExam.Name = "toolStripMenuItemNuevoExam";
            this.toolStripMenuItemNuevoExam.Size = new System.Drawing.Size(300, 30);
            this.toolStripMenuItemNuevoExam.Text = "Nueva Declaración";
            this.toolStripMenuItemNuevoExam.Click += new System.EventHandler(this.toolStripMenuItemNuevoExam_Click);
            // 
            // exportarPDFToolStripMenuItem1
            // 
            this.exportarPDFToolStripMenuItem1.AutoSize = false;
            this.exportarPDFToolStripMenuItem1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(50)))), ((int)(((byte)(73)))));
            this.exportarPDFToolStripMenuItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.exportarPDFToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.exportarPDFToolStripMenuItem1.Name = "exportarPDFToolStripMenuItem1";
            this.exportarPDFToolStripMenuItem1.Size = new System.Drawing.Size(300, 30);
            this.exportarPDFToolStripMenuItem1.Text = "Exportar PDF...";
            this.exportarPDFToolStripMenuItem1.Click += new System.EventHandler(this.exportarPDFToolStripMenuItem1_Click);
            // 
            // toolStripMenuItemImprimir
            // 
            this.toolStripMenuItemImprimir.AutoSize = false;
            this.toolStripMenuItemImprimir.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(50)))), ((int)(((byte)(73)))));
            this.toolStripMenuItemImprimir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripMenuItemImprimir.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemImprimir.Name = "toolStripMenuItemImprimir";
            this.toolStripMenuItemImprimir.Size = new System.Drawing.Size(300, 30);
            this.toolStripMenuItemImprimir.Text = "Imprimir";
            this.toolStripMenuItemImprimir.Click += new System.EventHandler(this.toolStripMenuItemImprimir_Click);
            // 
            // toolStripMenuItemSalir
            // 
            this.toolStripMenuItemSalir.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(50)))), ((int)(((byte)(73)))));
            this.toolStripMenuItemSalir.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemSalir.Name = "toolStripMenuItemSalir";
            this.toolStripMenuItemSalir.Size = new System.Drawing.Size(211, 24);
            this.toolStripMenuItemSalir.Text = "Salir";
            this.toolStripMenuItemSalir.Click += new System.EventHandler(this.toolStripMenuItemSalir_Click);
            // 
            // herramientasToolStripMenuItem
            // 
            this.herramientasToolStripMenuItem.AutoSize = false;
            this.herramientasToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(50)))), ((int)(((byte)(73)))));
            this.herramientasToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.herramientasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configuraciónToolStripMenuItem});
            this.herramientasToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.herramientasToolStripMenuItem.Name = "herramientasToolStripMenuItem";
            this.herramientasToolStripMenuItem.Size = new System.Drawing.Size(300, 30);
            this.herramientasToolStripMenuItem.Text = "&Herramientas";
            this.herramientasToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // configuraciónToolStripMenuItem
            // 
            this.configuraciónToolStripMenuItem.AutoSize = false;
            this.configuraciónToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(50)))), ((int)(((byte)(73)))));
            this.configuraciónToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.configuraciónToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.configuraciónToolStripMenuItem.Name = "configuraciónToolStripMenuItem";
            this.configuraciónToolStripMenuItem.Size = new System.Drawing.Size(300, 30);
            this.configuraciónToolStripMenuItem.Text = "&Configuración...";
            this.configuraciónToolStripMenuItem.Click += new System.EventHandler(this.configuraciónToolStripMenuItem_Click);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.salirToolStripMenuItem.AutoToolTip = true;
            this.salirToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.salirToolStripMenuItem.Image = global::rocadeclaracion.Properties.Resources.salida;
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(44, 36);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.ToolTipText = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // imprimirtoolStripMenuItem
            // 
            this.imprimirtoolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.imprimirtoolStripMenuItem.AutoToolTip = true;
            this.imprimirtoolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.imprimirtoolStripMenuItem.Image = global::rocadeclaracion.Properties.Resources.imprimir;
            this.imprimirtoolStripMenuItem.Name = "imprimirtoolStripMenuItem";
            this.imprimirtoolStripMenuItem.Size = new System.Drawing.Size(44, 36);
            this.imprimirtoolStripMenuItem.Text = "Imprimir";
            this.imprimirtoolStripMenuItem.Click += new System.EventHandler(this.imprimirtoolStripMenuItem_Click);
            // 
            // exportarpdftoolStripMenuItem
            // 
            this.exportarpdftoolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.exportarpdftoolStripMenuItem.AutoToolTip = true;
            this.exportarpdftoolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.exportarpdftoolStripMenuItem.Image = global::rocadeclaracion.Properties.Resources.guardapdf;
            this.exportarpdftoolStripMenuItem.Name = "exportarpdftoolStripMenuItem";
            this.exportarpdftoolStripMenuItem.Size = new System.Drawing.Size(44, 36);
            this.exportarpdftoolStripMenuItem.Text = "Exportar PDF...";
            this.exportarpdftoolStripMenuItem.Click += new System.EventHandler(this.exportarpdftoolStripMenuItem_Click);
            // 
            // nuevotoolStripMenuItem
            // 
            this.nuevotoolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.nuevotoolStripMenuItem.AutoToolTip = true;
            this.nuevotoolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.nuevotoolStripMenuItem.Image = global::rocadeclaracion.Properties.Resources.nuevo;
            this.nuevotoolStripMenuItem.Name = "nuevotoolStripMenuItem";
            this.nuevotoolStripMenuItem.Size = new System.Drawing.Size(44, 36);
            this.nuevotoolStripMenuItem.Text = "Nueva Declaración";
            this.nuevotoolStripMenuItem.Click += new System.EventHandler(this.nuevotoolStripMenuItem_Click);
            // 
            // separaciontoolStripMenuItem
            // 
            this.separaciontoolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.separaciontoolStripMenuItem.AutoSize = false;
            this.separaciontoolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.separaciontoolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.separaciontoolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.separaciontoolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.separaciontoolStripMenuItem.Name = "separaciontoolStripMenuItem";
            this.separaciontoolStripMenuItem.Size = new System.Drawing.Size(2, 30);
            this.separaciontoolStripMenuItem.Text = "toolStripMenuItem1";
            // 
            // nombretoolStripMenuItem
            // 
            this.nombretoolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.nombretoolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.nombretoolStripMenuItem.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nombretoolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.nombretoolStripMenuItem.Name = "nombretoolStripMenuItem";
            this.nombretoolStripMenuItem.Size = new System.Drawing.Size(163, 36);
            this.nombretoolStripMenuItem.Text = "Gustavo Juan Elena";
            // 
            // tituloVentanaToolStripMenuItem
            // 
            this.tituloVentanaToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tituloVentanaToolStripMenuItem.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Bold);
            this.tituloVentanaToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.tituloVentanaToolStripMenuItem.Name = "tituloVentanaToolStripMenuItem";
            this.tituloVentanaToolStripMenuItem.Size = new System.Drawing.Size(307, 36);
            this.tituloVentanaToolStripMenuItem.Text = "Declaración Jurada General Roca";
            // 
            // btnLeerDNI
            // 
            this.btnLeerDNI.FlatAppearance.BorderSize = 0;
            this.btnLeerDNI.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLeerDNI.Font = new System.Drawing.Font("Roboto", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLeerDNI.ForeColor = System.Drawing.Color.White;
            this.btnLeerDNI.Location = new System.Drawing.Point(398, 50);
            this.btnLeerDNI.Margin = new System.Windows.Forms.Padding(0);
            this.btnLeerDNI.Name = "btnLeerDNI";
            this.btnLeerDNI.Size = new System.Drawing.Size(225, 48);
            this.btnLeerDNI.TabIndex = 4;
            this.btnLeerDNI.Text = "Leer DNI";
            this.btnLeerDNI.UseVisualStyleBackColor = true;
            this.btnLeerDNI.Click += new System.EventHandler(this.btnLeerDNI_Click);
            // 
            // panelEntrevista
            // 
            this.panelEntrevista.AutoScroll = true;
            this.panelEntrevista.BackColor = System.Drawing.Color.Gray;
            this.panelEntrevista.Controls.Add(this.panelPreg21);
            this.panelEntrevista.Controls.Add(this.lblPreg21);
            this.panelEntrevista.Controls.Add(this.panelPreg20);
            this.panelEntrevista.Controls.Add(this.lblPreg20);
            this.panelEntrevista.Controls.Add(this.panelPreg19);
            this.panelEntrevista.Controls.Add(this.lblPreg19);
            this.panelEntrevista.Controls.Add(this.panelPreg18);
            this.panelEntrevista.Controls.Add(this.lblPreg18);
            this.panelEntrevista.Controls.Add(this.panelPreg17);
            this.panelEntrevista.Controls.Add(this.lblPreg17);
            this.panelEntrevista.Controls.Add(this.panelPreg16);
            this.panelEntrevista.Controls.Add(this.lblPreg16);
            this.panelEntrevista.Controls.Add(this.panelPreg15);
            this.panelEntrevista.Controls.Add(this.lblPreg15);
            this.panelEntrevista.Controls.Add(this.panelPreg14);
            this.panelEntrevista.Controls.Add(this.lblPreg14);
            this.panelEntrevista.Controls.Add(this.panelPreg13);
            this.panelEntrevista.Controls.Add(this.lblPreg13);
            this.panelEntrevista.Controls.Add(this.panelPreg12);
            this.panelEntrevista.Controls.Add(this.lblPreg12);
            this.panelEntrevista.Controls.Add(this.tbNacion);
            this.panelEntrevista.Controls.Add(this.lblNacion);
            this.panelEntrevista.Controls.Add(this.panelPreg11);
            this.panelEntrevista.Controls.Add(this.lblPreg11);
            this.panelEntrevista.Controls.Add(this.panelPreg10);
            this.panelEntrevista.Controls.Add(this.lblPreg10);
            this.panelEntrevista.Controls.Add(this.panelPreg9);
            this.panelEntrevista.Controls.Add(this.lblPreg9);
            this.panelEntrevista.Controls.Add(this.panelPreg8);
            this.panelEntrevista.Controls.Add(this.lblPreg8);
            this.panelEntrevista.Controls.Add(this.panelPreg7);
            this.panelEntrevista.Controls.Add(this.lblPreg7);
            this.panelEntrevista.Controls.Add(this.panelPreg6);
            this.panelEntrevista.Controls.Add(this.lblPreg6);
            this.panelEntrevista.Controls.Add(this.panelPreg5);
            this.panelEntrevista.Controls.Add(this.lblPreg5);
            this.panelEntrevista.Controls.Add(this.panelPreg4);
            this.panelEntrevista.Controls.Add(this.lblPreg4);
            this.panelEntrevista.Controls.Add(this.panelPreg3);
            this.panelEntrevista.Controls.Add(this.lblPreg3);
            this.panelEntrevista.Controls.Add(this.panelPreg2);
            this.panelEntrevista.Controls.Add(this.lblPreg2);
            this.panelEntrevista.Controls.Add(this.panelPreg1);
            this.panelEntrevista.Controls.Add(this.lblPreg1);
            this.panelEntrevista.Controls.Add(this.lblAntecedentes);
            this.panelEntrevista.Controls.Add(this.cmbFactor);
            this.panelEntrevista.Controls.Add(this.lblFactor);
            this.panelEntrevista.Controls.Add(this.cmbGrupo);
            this.panelEntrevista.Controls.Add(this.lblGrupo);
            this.panelEntrevista.Controls.Add(this.lblCm);
            this.panelEntrevista.Controls.Add(this.udTalla);
            this.panelEntrevista.Controls.Add(this.lblTalla);
            this.panelEntrevista.Controls.Add(this.lblKg);
            this.panelEntrevista.Controls.Add(this.udPeso);
            this.panelEntrevista.Controls.Add(this.lblPeso);
            this.panelEntrevista.Controls.Add(this.tbOcupacion);
            this.panelEntrevista.Controls.Add(this.lblOcupacion);
            this.panelEntrevista.Controls.Add(this.tbDirección);
            this.panelEntrevista.Controls.Add(this.lblDireccion);
            this.panelEntrevista.Controls.Add(this.rbSexoX);
            this.panelEntrevista.Controls.Add(this.rbSexoFem);
            this.panelEntrevista.Controls.Add(this.lblEdad);
            this.panelEntrevista.Controls.Add(this.rbSexoMasc);
            this.panelEntrevista.Controls.Add(this.dtpFechaNac);
            this.panelEntrevista.Controls.Add(this.dtpFechaEmision);
            this.panelEntrevista.Controls.Add(this.lblSexo);
            this.panelEntrevista.Controls.Add(this.lblFechaNac);
            this.panelEntrevista.Controls.Add(this.lblFechaEmision);
            this.panelEntrevista.Controls.Add(this.tbNoDoc);
            this.panelEntrevista.Controls.Add(this.cmbTipoDoc);
            this.panelEntrevista.Controls.Add(this.lblNoDoc);
            this.panelEntrevista.Controls.Add(this.lblTipoDoc);
            this.panelEntrevista.Controls.Add(this.tbNombre);
            this.panelEntrevista.Controls.Add(this.lblNombre);
            this.panelEntrevista.Controls.Add(this.tbApellido);
            this.panelEntrevista.Controls.Add(this.lblApellido);
            this.panelEntrevista.Controls.Add(this.lblTitEdad);
            this.panelEntrevista.Location = new System.Drawing.Point(12, 130);
            this.panelEntrevista.Name = "panelEntrevista";
            this.panelEntrevista.Size = new System.Drawing.Size(1027, 621);
            this.panelEntrevista.TabIndex = 5;
            // 
            // panelPreg21
            // 
            this.panelPreg21.Controls.Add(this.rbSi21);
            this.panelPreg21.Controls.Add(this.rbNo21);
            this.panelPreg21.Location = new System.Drawing.Point(767, 608);
            this.panelPreg21.Name = "panelPreg21";
            this.panelPreg21.Size = new System.Drawing.Size(106, 30);
            this.panelPreg21.TabIndex = 10097;
            // 
            // rbSi21
            // 
            this.rbSi21.AutoSize = true;
            this.rbSi21.BackColor = System.Drawing.Color.Transparent;
            this.rbSi21.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSi21.Location = new System.Drawing.Point(6, 3);
            this.rbSi21.Name = "rbSi21";
            this.rbSi21.Size = new System.Drawing.Size(41, 23);
            this.rbSi21.TabIndex = 10061;
            this.rbSi21.TabStop = true;
            this.rbSi21.Text = "SI";
            this.rbSi21.UseVisualStyleBackColor = false;
            this.rbSi21.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // rbNo21
            // 
            this.rbNo21.AutoSize = true;
            this.rbNo21.BackColor = System.Drawing.Color.Transparent;
            this.rbNo21.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbNo21.Location = new System.Drawing.Point(53, 3);
            this.rbNo21.Name = "rbNo21";
            this.rbNo21.Size = new System.Drawing.Size(49, 23);
            this.rbNo21.TabIndex = 10062;
            this.rbNo21.TabStop = true;
            this.rbNo21.Text = "NO";
            this.rbNo21.UseVisualStyleBackColor = false;
            this.rbNo21.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblPreg21
            // 
            this.lblPreg21.BackColor = System.Drawing.Color.Transparent;
            this.lblPreg21.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreg21.Location = new System.Drawing.Point(450, 611);
            this.lblPreg21.Name = "lblPreg21";
            this.lblPreg21.Size = new System.Drawing.Size(317, 21);
            this.lblPreg21.TabIndex = 10096;
            this.lblPreg21.Text = "Intervenciones quirúrgicas:";
            this.lblPreg21.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelPreg20
            // 
            this.panelPreg20.Controls.Add(this.rbSi20);
            this.panelPreg20.Controls.Add(this.rbNo20);
            this.panelPreg20.Location = new System.Drawing.Point(767, 576);
            this.panelPreg20.Name = "panelPreg20";
            this.panelPreg20.Size = new System.Drawing.Size(106, 30);
            this.panelPreg20.TabIndex = 10095;
            // 
            // rbSi20
            // 
            this.rbSi20.AutoSize = true;
            this.rbSi20.BackColor = System.Drawing.Color.Transparent;
            this.rbSi20.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSi20.Location = new System.Drawing.Point(6, 3);
            this.rbSi20.Name = "rbSi20";
            this.rbSi20.Size = new System.Drawing.Size(41, 23);
            this.rbSi20.TabIndex = 10061;
            this.rbSi20.TabStop = true;
            this.rbSi20.Text = "SI";
            this.rbSi20.UseVisualStyleBackColor = false;
            this.rbSi20.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // rbNo20
            // 
            this.rbNo20.AutoSize = true;
            this.rbNo20.BackColor = System.Drawing.Color.Transparent;
            this.rbNo20.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbNo20.Location = new System.Drawing.Point(53, 3);
            this.rbNo20.Name = "rbNo20";
            this.rbNo20.Size = new System.Drawing.Size(49, 23);
            this.rbNo20.TabIndex = 10062;
            this.rbNo20.TabStop = true;
            this.rbNo20.Text = "NO";
            this.rbNo20.UseVisualStyleBackColor = false;
            this.rbNo20.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblPreg20
            // 
            this.lblPreg20.BackColor = System.Drawing.Color.Transparent;
            this.lblPreg20.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreg20.Location = new System.Drawing.Point(450, 579);
            this.lblPreg20.Name = "lblPreg20";
            this.lblPreg20.Size = new System.Drawing.Size(317, 21);
            this.lblPreg20.TabIndex = 10094;
            this.lblPreg20.Text = "Incapacidad laborativa permanente:";
            this.lblPreg20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelPreg19
            // 
            this.panelPreg19.Controls.Add(this.rbSi19);
            this.panelPreg19.Controls.Add(this.rbNo19);
            this.panelPreg19.Location = new System.Drawing.Point(767, 544);
            this.panelPreg19.Name = "panelPreg19";
            this.panelPreg19.Size = new System.Drawing.Size(106, 30);
            this.panelPreg19.TabIndex = 10093;
            // 
            // rbSi19
            // 
            this.rbSi19.AutoSize = true;
            this.rbSi19.BackColor = System.Drawing.Color.Transparent;
            this.rbSi19.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSi19.Location = new System.Drawing.Point(6, 3);
            this.rbSi19.Name = "rbSi19";
            this.rbSi19.Size = new System.Drawing.Size(41, 23);
            this.rbSi19.TabIndex = 10061;
            this.rbSi19.TabStop = true;
            this.rbSi19.Text = "SI";
            this.rbSi19.UseVisualStyleBackColor = false;
            this.rbSi19.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // rbNo19
            // 
            this.rbNo19.AutoSize = true;
            this.rbNo19.BackColor = System.Drawing.Color.Transparent;
            this.rbNo19.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbNo19.Location = new System.Drawing.Point(53, 3);
            this.rbNo19.Name = "rbNo19";
            this.rbNo19.Size = new System.Drawing.Size(49, 23);
            this.rbNo19.TabIndex = 10062;
            this.rbNo19.TabStop = true;
            this.rbNo19.Text = "NO";
            this.rbNo19.UseVisualStyleBackColor = false;
            this.rbNo19.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblPreg19
            // 
            this.lblPreg19.BackColor = System.Drawing.Color.Transparent;
            this.lblPreg19.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreg19.Location = new System.Drawing.Point(450, 547);
            this.lblPreg19.Name = "lblPreg19";
            this.lblPreg19.Size = new System.Drawing.Size(317, 21);
            this.lblPreg19.TabIndex = 10092;
            this.lblPreg19.Text = "Usa audífonos:";
            this.lblPreg19.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelPreg18
            // 
            this.panelPreg18.Controls.Add(this.rbSi18);
            this.panelPreg18.Controls.Add(this.rbNo18);
            this.panelPreg18.Location = new System.Drawing.Point(767, 512);
            this.panelPreg18.Name = "panelPreg18";
            this.panelPreg18.Size = new System.Drawing.Size(106, 30);
            this.panelPreg18.TabIndex = 10091;
            // 
            // rbSi18
            // 
            this.rbSi18.AutoSize = true;
            this.rbSi18.BackColor = System.Drawing.Color.Transparent;
            this.rbSi18.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSi18.Location = new System.Drawing.Point(6, 3);
            this.rbSi18.Name = "rbSi18";
            this.rbSi18.Size = new System.Drawing.Size(41, 23);
            this.rbSi18.TabIndex = 10061;
            this.rbSi18.TabStop = true;
            this.rbSi18.Text = "SI";
            this.rbSi18.UseVisualStyleBackColor = false;
            this.rbSi18.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // rbNo18
            // 
            this.rbNo18.AutoSize = true;
            this.rbNo18.BackColor = System.Drawing.Color.Transparent;
            this.rbNo18.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbNo18.Location = new System.Drawing.Point(53, 3);
            this.rbNo18.Name = "rbNo18";
            this.rbNo18.Size = new System.Drawing.Size(49, 23);
            this.rbNo18.TabIndex = 10062;
            this.rbNo18.TabStop = true;
            this.rbNo18.Text = "NO";
            this.rbNo18.UseVisualStyleBackColor = false;
            this.rbNo18.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblPreg18
            // 
            this.lblPreg18.BackColor = System.Drawing.Color.Transparent;
            this.lblPreg18.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreg18.Location = new System.Drawing.Point(450, 515);
            this.lblPreg18.Name = "lblPreg18";
            this.lblPreg18.Size = new System.Drawing.Size(317, 21);
            this.lblPreg18.TabIndex = 10090;
            this.lblPreg18.Text = "Otras sustancias psicoactivas:";
            this.lblPreg18.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelPreg17
            // 
            this.panelPreg17.Controls.Add(this.rbSi17);
            this.panelPreg17.Controls.Add(this.rbNo17);
            this.panelPreg17.Location = new System.Drawing.Point(767, 480);
            this.panelPreg17.Name = "panelPreg17";
            this.panelPreg17.Size = new System.Drawing.Size(106, 30);
            this.panelPreg17.TabIndex = 10089;
            // 
            // rbSi17
            // 
            this.rbSi17.AutoSize = true;
            this.rbSi17.BackColor = System.Drawing.Color.Transparent;
            this.rbSi17.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSi17.Location = new System.Drawing.Point(6, 3);
            this.rbSi17.Name = "rbSi17";
            this.rbSi17.Size = new System.Drawing.Size(41, 23);
            this.rbSi17.TabIndex = 10061;
            this.rbSi17.TabStop = true;
            this.rbSi17.Text = "SI";
            this.rbSi17.UseVisualStyleBackColor = false;
            this.rbSi17.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // rbNo17
            // 
            this.rbNo17.AutoSize = true;
            this.rbNo17.BackColor = System.Drawing.Color.Transparent;
            this.rbNo17.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbNo17.Location = new System.Drawing.Point(53, 3);
            this.rbNo17.Name = "rbNo17";
            this.rbNo17.Size = new System.Drawing.Size(49, 23);
            this.rbNo17.TabIndex = 10062;
            this.rbNo17.TabStop = true;
            this.rbNo17.Text = "NO";
            this.rbNo17.UseVisualStyleBackColor = false;
            this.rbNo17.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblPreg17
            // 
            this.lblPreg17.BackColor = System.Drawing.Color.Transparent;
            this.lblPreg17.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreg17.Location = new System.Drawing.Point(450, 483);
            this.lblPreg17.Name = "lblPreg17";
            this.lblPreg17.Size = new System.Drawing.Size(317, 21);
            this.lblPreg17.TabIndex = 10088;
            this.lblPreg17.Text = "Tabaco:";
            this.lblPreg17.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelPreg16
            // 
            this.panelPreg16.Controls.Add(this.rbSi16);
            this.panelPreg16.Controls.Add(this.rbNo16);
            this.panelPreg16.Location = new System.Drawing.Point(767, 448);
            this.panelPreg16.Name = "panelPreg16";
            this.panelPreg16.Size = new System.Drawing.Size(106, 30);
            this.panelPreg16.TabIndex = 10087;
            // 
            // rbSi16
            // 
            this.rbSi16.AutoSize = true;
            this.rbSi16.BackColor = System.Drawing.Color.Transparent;
            this.rbSi16.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSi16.Location = new System.Drawing.Point(6, 3);
            this.rbSi16.Name = "rbSi16";
            this.rbSi16.Size = new System.Drawing.Size(41, 23);
            this.rbSi16.TabIndex = 10061;
            this.rbSi16.TabStop = true;
            this.rbSi16.Text = "SI";
            this.rbSi16.UseVisualStyleBackColor = false;
            this.rbSi16.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // rbNo16
            // 
            this.rbNo16.AutoSize = true;
            this.rbNo16.BackColor = System.Drawing.Color.Transparent;
            this.rbNo16.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbNo16.Location = new System.Drawing.Point(53, 3);
            this.rbNo16.Name = "rbNo16";
            this.rbNo16.Size = new System.Drawing.Size(49, 23);
            this.rbNo16.TabIndex = 10062;
            this.rbNo16.TabStop = true;
            this.rbNo16.Text = "NO";
            this.rbNo16.UseVisualStyleBackColor = false;
            this.rbNo16.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblPreg16
            // 
            this.lblPreg16.BackColor = System.Drawing.Color.Transparent;
            this.lblPreg16.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreg16.Location = new System.Drawing.Point(450, 451);
            this.lblPreg16.Name = "lblPreg16";
            this.lblPreg16.Size = new System.Drawing.Size(317, 21);
            this.lblPreg16.TabIndex = 10086;
            this.lblPreg16.Text = "Consumo de alcohol:";
            this.lblPreg16.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelPreg15
            // 
            this.panelPreg15.Controls.Add(this.rbSi15);
            this.panelPreg15.Controls.Add(this.rbNo15);
            this.panelPreg15.Location = new System.Drawing.Point(767, 416);
            this.panelPreg15.Name = "panelPreg15";
            this.panelPreg15.Size = new System.Drawing.Size(106, 30);
            this.panelPreg15.TabIndex = 10085;
            // 
            // rbSi15
            // 
            this.rbSi15.AutoSize = true;
            this.rbSi15.BackColor = System.Drawing.Color.Transparent;
            this.rbSi15.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSi15.Location = new System.Drawing.Point(6, 3);
            this.rbSi15.Name = "rbSi15";
            this.rbSi15.Size = new System.Drawing.Size(41, 23);
            this.rbSi15.TabIndex = 10061;
            this.rbSi15.TabStop = true;
            this.rbSi15.Text = "SI";
            this.rbSi15.UseVisualStyleBackColor = false;
            this.rbSi15.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // rbNo15
            // 
            this.rbNo15.AutoSize = true;
            this.rbNo15.BackColor = System.Drawing.Color.Transparent;
            this.rbNo15.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbNo15.Location = new System.Drawing.Point(53, 3);
            this.rbNo15.Name = "rbNo15";
            this.rbNo15.Size = new System.Drawing.Size(49, 23);
            this.rbNo15.TabIndex = 10062;
            this.rbNo15.TabStop = true;
            this.rbNo15.Text = "NO";
            this.rbNo15.UseVisualStyleBackColor = false;
            this.rbNo15.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblPreg15
            // 
            this.lblPreg15.BackColor = System.Drawing.Color.Transparent;
            this.lblPreg15.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreg15.Location = new System.Drawing.Point(450, 419);
            this.lblPreg15.Name = "lblPreg15";
            this.lblPreg15.Size = new System.Drawing.Size(317, 21);
            this.lblPreg15.TabIndex = 10084;
            this.lblPreg15.Text = "Uso habitual de psicofármacos:";
            this.lblPreg15.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelPreg14
            // 
            this.panelPreg14.Controls.Add(this.rbSi14);
            this.panelPreg14.Controls.Add(this.rbNo14);
            this.panelPreg14.Location = new System.Drawing.Point(767, 383);
            this.panelPreg14.Name = "panelPreg14";
            this.panelPreg14.Size = new System.Drawing.Size(106, 30);
            this.panelPreg14.TabIndex = 10067;
            // 
            // rbSi14
            // 
            this.rbSi14.AutoSize = true;
            this.rbSi14.BackColor = System.Drawing.Color.Transparent;
            this.rbSi14.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSi14.Location = new System.Drawing.Point(6, 3);
            this.rbSi14.Name = "rbSi14";
            this.rbSi14.Size = new System.Drawing.Size(41, 23);
            this.rbSi14.TabIndex = 10061;
            this.rbSi14.TabStop = true;
            this.rbSi14.Text = "SI";
            this.rbSi14.UseVisualStyleBackColor = false;
            this.rbSi14.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // rbNo14
            // 
            this.rbNo14.AutoSize = true;
            this.rbNo14.BackColor = System.Drawing.Color.Transparent;
            this.rbNo14.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbNo14.Location = new System.Drawing.Point(53, 3);
            this.rbNo14.Name = "rbNo14";
            this.rbNo14.Size = new System.Drawing.Size(49, 23);
            this.rbNo14.TabIndex = 10062;
            this.rbNo14.TabStop = true;
            this.rbNo14.Text = "NO";
            this.rbNo14.UseVisualStyleBackColor = false;
            this.rbNo14.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblPreg14
            // 
            this.lblPreg14.BackColor = System.Drawing.Color.Transparent;
            this.lblPreg14.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreg14.Location = new System.Drawing.Point(450, 386);
            this.lblPreg14.Name = "lblPreg14";
            this.lblPreg14.Size = new System.Drawing.Size(317, 21);
            this.lblPreg14.TabIndex = 10066;
            this.lblPreg14.Text = "Trastornos mentales:";
            this.lblPreg14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelPreg13
            // 
            this.panelPreg13.Controls.Add(this.rbSi13);
            this.panelPreg13.Controls.Add(this.rbNo13);
            this.panelPreg13.Location = new System.Drawing.Point(767, 331);
            this.panelPreg13.Name = "panelPreg13";
            this.panelPreg13.Size = new System.Drawing.Size(106, 30);
            this.panelPreg13.TabIndex = 10067;
            // 
            // rbSi13
            // 
            this.rbSi13.AutoSize = true;
            this.rbSi13.BackColor = System.Drawing.Color.Transparent;
            this.rbSi13.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSi13.Location = new System.Drawing.Point(6, 3);
            this.rbSi13.Name = "rbSi13";
            this.rbSi13.Size = new System.Drawing.Size(41, 23);
            this.rbSi13.TabIndex = 10061;
            this.rbSi13.TabStop = true;
            this.rbSi13.Text = "SI";
            this.rbSi13.UseVisualStyleBackColor = false;
            this.rbSi13.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // rbNo13
            // 
            this.rbNo13.AutoSize = true;
            this.rbNo13.BackColor = System.Drawing.Color.Transparent;
            this.rbNo13.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbNo13.Location = new System.Drawing.Point(53, 3);
            this.rbNo13.Name = "rbNo13";
            this.rbNo13.Size = new System.Drawing.Size(49, 23);
            this.rbNo13.TabIndex = 10062;
            this.rbNo13.TabStop = true;
            this.rbNo13.Text = "NO";
            this.rbNo13.UseVisualStyleBackColor = false;
            this.rbNo13.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblPreg13
            // 
            this.lblPreg13.BackColor = System.Drawing.Color.Transparent;
            this.lblPreg13.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreg13.Location = new System.Drawing.Point(423, 317);
            this.lblPreg13.Name = "lblPreg13";
            this.lblPreg13.Size = new System.Drawing.Size(344, 82);
            this.lblPreg13.TabIndex = 10066;
            this.lblPreg13.Text = "Trastornos neurológicos: Epilepsia, Trastornos de la locomoción o la sensibilidad" +
    ", parálisis, secuelas de traumatismo:";
            this.lblPreg13.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelPreg12
            // 
            this.panelPreg12.Controls.Add(this.rbSi12);
            this.panelPreg12.Controls.Add(this.rbNo12);
            this.panelPreg12.Location = new System.Drawing.Point(767, 284);
            this.panelPreg12.Name = "panelPreg12";
            this.panelPreg12.Size = new System.Drawing.Size(106, 30);
            this.panelPreg12.TabIndex = 10065;
            // 
            // rbSi12
            // 
            this.rbSi12.AutoSize = true;
            this.rbSi12.BackColor = System.Drawing.Color.Transparent;
            this.rbSi12.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSi12.Location = new System.Drawing.Point(6, 3);
            this.rbSi12.Name = "rbSi12";
            this.rbSi12.Size = new System.Drawing.Size(41, 23);
            this.rbSi12.TabIndex = 10061;
            this.rbSi12.TabStop = true;
            this.rbSi12.Text = "SI";
            this.rbSi12.UseVisualStyleBackColor = false;
            this.rbSi12.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // rbNo12
            // 
            this.rbNo12.AutoSize = true;
            this.rbNo12.BackColor = System.Drawing.Color.Transparent;
            this.rbNo12.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbNo12.Location = new System.Drawing.Point(53, 3);
            this.rbNo12.Name = "rbNo12";
            this.rbNo12.Size = new System.Drawing.Size(49, 23);
            this.rbNo12.TabIndex = 10062;
            this.rbNo12.TabStop = true;
            this.rbNo12.Text = "NO";
            this.rbNo12.UseVisualStyleBackColor = false;
            this.rbNo12.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblPreg12
            // 
            this.lblPreg12.BackColor = System.Drawing.Color.Transparent;
            this.lblPreg12.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreg12.Location = new System.Drawing.Point(450, 287);
            this.lblPreg12.Name = "lblPreg12";
            this.lblPreg12.Size = new System.Drawing.Size(317, 21);
            this.lblPreg12.TabIndex = 10064;
            this.lblPreg12.Text = "Mareos o desmayos:";
            this.lblPreg12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tbNacion
            // 
            this.tbNacion.BackColor = System.Drawing.Color.LightGray;
            this.tbNacion.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbNacion.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNacion.Location = new System.Drawing.Point(162, 66);
            this.tbNacion.Name = "tbNacion";
            this.tbNacion.Size = new System.Drawing.Size(311, 20);
            this.tbNacion.TabIndex = 10083;
            this.tbNacion.TextChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblNacion
            // 
            this.lblNacion.BackColor = System.Drawing.Color.Transparent;
            this.lblNacion.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNacion.Location = new System.Drawing.Point(11, 66);
            this.lblNacion.Name = "lblNacion";
            this.lblNacion.Size = new System.Drawing.Size(145, 21);
            this.lblNacion.TabIndex = 10082;
            this.lblNacion.Text = "Nacionalidad:";
            this.lblNacion.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelPreg11
            // 
            this.panelPreg11.Controls.Add(this.rbSi11);
            this.panelPreg11.Controls.Add(this.rbNo11);
            this.panelPreg11.Location = new System.Drawing.Point(311, 619);
            this.panelPreg11.Name = "panelPreg11";
            this.panelPreg11.Size = new System.Drawing.Size(106, 30);
            this.panelPreg11.TabIndex = 10081;
            // 
            // rbSi11
            // 
            this.rbSi11.AutoSize = true;
            this.rbSi11.BackColor = System.Drawing.Color.Transparent;
            this.rbSi11.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSi11.Location = new System.Drawing.Point(6, 3);
            this.rbSi11.Name = "rbSi11";
            this.rbSi11.Size = new System.Drawing.Size(41, 23);
            this.rbSi11.TabIndex = 10061;
            this.rbSi11.TabStop = true;
            this.rbSi11.Text = "SI";
            this.rbSi11.UseVisualStyleBackColor = false;
            this.rbSi11.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // rbNo11
            // 
            this.rbNo11.AutoSize = true;
            this.rbNo11.BackColor = System.Drawing.Color.Transparent;
            this.rbNo11.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbNo11.Location = new System.Drawing.Point(53, 3);
            this.rbNo11.Name = "rbNo11";
            this.rbNo11.Size = new System.Drawing.Size(49, 23);
            this.rbNo11.TabIndex = 10062;
            this.rbNo11.TabStop = true;
            this.rbNo11.Text = "NO";
            this.rbNo11.UseVisualStyleBackColor = false;
            this.rbNo11.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblPreg11
            // 
            this.lblPreg11.BackColor = System.Drawing.Color.Transparent;
            this.lblPreg11.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreg11.Location = new System.Drawing.Point(11, 622);
            this.lblPreg11.Name = "lblPreg11";
            this.lblPreg11.Size = new System.Drawing.Size(291, 21);
            this.lblPreg11.TabIndex = 10080;
            this.lblPreg11.Text = "Certificación de Discapacidad (Ley 2055)";
            this.lblPreg11.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelPreg10
            // 
            this.panelPreg10.Controls.Add(this.rbSi10);
            this.panelPreg10.Controls.Add(this.rbNo10);
            this.panelPreg10.Location = new System.Drawing.Point(311, 587);
            this.panelPreg10.Name = "panelPreg10";
            this.panelPreg10.Size = new System.Drawing.Size(106, 30);
            this.panelPreg10.TabIndex = 10079;
            // 
            // rbSi10
            // 
            this.rbSi10.AutoSize = true;
            this.rbSi10.BackColor = System.Drawing.Color.Transparent;
            this.rbSi10.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSi10.Location = new System.Drawing.Point(6, 3);
            this.rbSi10.Name = "rbSi10";
            this.rbSi10.Size = new System.Drawing.Size(41, 23);
            this.rbSi10.TabIndex = 10061;
            this.rbSi10.TabStop = true;
            this.rbSi10.Text = "SI";
            this.rbSi10.UseVisualStyleBackColor = false;
            this.rbSi10.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // rbNo10
            // 
            this.rbNo10.AutoSize = true;
            this.rbNo10.BackColor = System.Drawing.Color.Transparent;
            this.rbNo10.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbNo10.Location = new System.Drawing.Point(53, 3);
            this.rbNo10.Name = "rbNo10";
            this.rbNo10.Size = new System.Drawing.Size(49, 23);
            this.rbNo10.TabIndex = 10062;
            this.rbNo10.TabStop = true;
            this.rbNo10.Text = "NO";
            this.rbNo10.UseVisualStyleBackColor = false;
            this.rbNo10.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblPreg10
            // 
            this.lblPreg10.BackColor = System.Drawing.Color.Transparent;
            this.lblPreg10.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreg10.Location = new System.Drawing.Point(11, 590);
            this.lblPreg10.Name = "lblPreg10";
            this.lblPreg10.Size = new System.Drawing.Size(291, 21);
            this.lblPreg10.TabIndex = 10078;
            this.lblPreg10.Text = "Ha sido rechazado por algún seguro de vida:";
            this.lblPreg10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelPreg9
            // 
            this.panelPreg9.Controls.Add(this.rbSi9);
            this.panelPreg9.Controls.Add(this.rbNo9);
            this.panelPreg9.Location = new System.Drawing.Point(311, 555);
            this.panelPreg9.Name = "panelPreg9";
            this.panelPreg9.Size = new System.Drawing.Size(106, 30);
            this.panelPreg9.TabIndex = 10077;
            // 
            // rbSi9
            // 
            this.rbSi9.AutoSize = true;
            this.rbSi9.BackColor = System.Drawing.Color.Transparent;
            this.rbSi9.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSi9.Location = new System.Drawing.Point(6, 3);
            this.rbSi9.Name = "rbSi9";
            this.rbSi9.Size = new System.Drawing.Size(41, 23);
            this.rbSi9.TabIndex = 10061;
            this.rbSi9.TabStop = true;
            this.rbSi9.Text = "SI";
            this.rbSi9.UseVisualStyleBackColor = false;
            this.rbSi9.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // rbNo9
            // 
            this.rbNo9.AutoSize = true;
            this.rbNo9.BackColor = System.Drawing.Color.Transparent;
            this.rbNo9.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbNo9.Location = new System.Drawing.Point(53, 3);
            this.rbNo9.Name = "rbNo9";
            this.rbNo9.Size = new System.Drawing.Size(49, 23);
            this.rbNo9.TabIndex = 10062;
            this.rbNo9.TabStop = true;
            this.rbNo9.Text = "NO";
            this.rbNo9.UseVisualStyleBackColor = false;
            this.rbNo9.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblPreg9
            // 
            this.lblPreg9.BackColor = System.Drawing.Color.Transparent;
            this.lblPreg9.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreg9.Location = new System.Drawing.Point(22, 558);
            this.lblPreg9.Name = "lblPreg9";
            this.lblPreg9.Size = new System.Drawing.Size(280, 21);
            this.lblPreg9.TabIndex = 10076;
            this.lblPreg9.Text = "Usa anteojos de Aumento:";
            this.lblPreg9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelPreg8
            // 
            this.panelPreg8.Controls.Add(this.rbSi8);
            this.panelPreg8.Controls.Add(this.rbNo8);
            this.panelPreg8.Location = new System.Drawing.Point(311, 508);
            this.panelPreg8.Name = "panelPreg8";
            this.panelPreg8.Size = new System.Drawing.Size(106, 45);
            this.panelPreg8.TabIndex = 10077;
            // 
            // rbSi8
            // 
            this.rbSi8.AutoSize = true;
            this.rbSi8.BackColor = System.Drawing.Color.Transparent;
            this.rbSi8.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSi8.Location = new System.Drawing.Point(6, 13);
            this.rbSi8.Name = "rbSi8";
            this.rbSi8.Size = new System.Drawing.Size(41, 23);
            this.rbSi8.TabIndex = 10061;
            this.rbSi8.TabStop = true;
            this.rbSi8.Text = "SI";
            this.rbSi8.UseVisualStyleBackColor = false;
            this.rbSi8.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // rbNo8
            // 
            this.rbNo8.AutoSize = true;
            this.rbNo8.BackColor = System.Drawing.Color.Transparent;
            this.rbNo8.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbNo8.Location = new System.Drawing.Point(53, 13);
            this.rbNo8.Name = "rbNo8";
            this.rbNo8.Size = new System.Drawing.Size(49, 23);
            this.rbNo8.TabIndex = 10062;
            this.rbNo8.TabStop = true;
            this.rbNo8.Text = "NO";
            this.rbNo8.UseVisualStyleBackColor = false;
            this.rbNo8.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblPreg8
            // 
            this.lblPreg8.BackColor = System.Drawing.Color.Transparent;
            this.lblPreg8.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreg8.Location = new System.Drawing.Point(13, 511);
            this.lblPreg8.Name = "lblPreg8";
            this.lblPreg8.Size = new System.Drawing.Size(289, 42);
            this.lblPreg8.TabIndex = 10076;
            this.lblPreg8.Text = "Problemas con la Ley ligado a uso o abuso de sustancias:";
            this.lblPreg8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelPreg7
            // 
            this.panelPreg7.Controls.Add(this.rbSi7);
            this.panelPreg7.Controls.Add(this.rbNo7);
            this.panelPreg7.Location = new System.Drawing.Point(311, 476);
            this.panelPreg7.Name = "panelPreg7";
            this.panelPreg7.Size = new System.Drawing.Size(106, 30);
            this.panelPreg7.TabIndex = 10075;
            // 
            // rbSi7
            // 
            this.rbSi7.AutoSize = true;
            this.rbSi7.BackColor = System.Drawing.Color.Transparent;
            this.rbSi7.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSi7.Location = new System.Drawing.Point(6, 3);
            this.rbSi7.Name = "rbSi7";
            this.rbSi7.Size = new System.Drawing.Size(41, 23);
            this.rbSi7.TabIndex = 10061;
            this.rbSi7.TabStop = true;
            this.rbSi7.Text = "SI";
            this.rbSi7.UseVisualStyleBackColor = false;
            this.rbSi7.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // rbNo7
            // 
            this.rbNo7.AutoSize = true;
            this.rbNo7.BackColor = System.Drawing.Color.Transparent;
            this.rbNo7.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbNo7.Location = new System.Drawing.Point(53, 3);
            this.rbNo7.Name = "rbNo7";
            this.rbNo7.Size = new System.Drawing.Size(49, 23);
            this.rbNo7.TabIndex = 10062;
            this.rbNo7.TabStop = true;
            this.rbNo7.Text = "NO";
            this.rbNo7.UseVisualStyleBackColor = false;
            this.rbNo7.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblPreg7
            // 
            this.lblPreg7.BackColor = System.Drawing.Color.Transparent;
            this.lblPreg7.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreg7.Location = new System.Drawing.Point(22, 479);
            this.lblPreg7.Name = "lblPreg7";
            this.lblPreg7.Size = new System.Drawing.Size(280, 21);
            this.lblPreg7.TabIndex = 10074;
            this.lblPreg7.Text = "Medicamentos de uso permanente:";
            this.lblPreg7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelPreg6
            // 
            this.panelPreg6.Controls.Add(this.rbSi6);
            this.panelPreg6.Controls.Add(this.rbNo6);
            this.panelPreg6.Location = new System.Drawing.Point(311, 444);
            this.panelPreg6.Name = "panelPreg6";
            this.panelPreg6.Size = new System.Drawing.Size(106, 30);
            this.panelPreg6.TabIndex = 10073;
            // 
            // rbSi6
            // 
            this.rbSi6.AutoSize = true;
            this.rbSi6.BackColor = System.Drawing.Color.Transparent;
            this.rbSi6.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSi6.Location = new System.Drawing.Point(6, 3);
            this.rbSi6.Name = "rbSi6";
            this.rbSi6.Size = new System.Drawing.Size(41, 23);
            this.rbSi6.TabIndex = 10061;
            this.rbSi6.TabStop = true;
            this.rbSi6.Text = "SI";
            this.rbSi6.UseVisualStyleBackColor = false;
            this.rbSi6.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // rbNo6
            // 
            this.rbNo6.AutoSize = true;
            this.rbNo6.BackColor = System.Drawing.Color.Transparent;
            this.rbNo6.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbNo6.Location = new System.Drawing.Point(53, 3);
            this.rbNo6.Name = "rbNo6";
            this.rbNo6.Size = new System.Drawing.Size(49, 23);
            this.rbNo6.TabIndex = 10062;
            this.rbNo6.TabStop = true;
            this.rbNo6.Text = "NO";
            this.rbNo6.UseVisualStyleBackColor = false;
            this.rbNo6.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblPreg6
            // 
            this.lblPreg6.BackColor = System.Drawing.Color.Transparent;
            this.lblPreg6.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreg6.Location = new System.Drawing.Point(22, 447);
            this.lblPreg6.Name = "lblPreg6";
            this.lblPreg6.Size = new System.Drawing.Size(280, 21);
            this.lblPreg6.TabIndex = 10072;
            this.lblPreg6.Text = "Afecciones y operaciones oftalmológicas:";
            this.lblPreg6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelPreg5
            // 
            this.panelPreg5.Controls.Add(this.rbSi5);
            this.panelPreg5.Controls.Add(this.rbNo5);
            this.panelPreg5.Location = new System.Drawing.Point(311, 412);
            this.panelPreg5.Name = "panelPreg5";
            this.panelPreg5.Size = new System.Drawing.Size(106, 30);
            this.panelPreg5.TabIndex = 10071;
            // 
            // rbSi5
            // 
            this.rbSi5.AutoSize = true;
            this.rbSi5.BackColor = System.Drawing.Color.Transparent;
            this.rbSi5.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSi5.Location = new System.Drawing.Point(6, 3);
            this.rbSi5.Name = "rbSi5";
            this.rbSi5.Size = new System.Drawing.Size(41, 23);
            this.rbSi5.TabIndex = 10061;
            this.rbSi5.TabStop = true;
            this.rbSi5.Text = "SI";
            this.rbSi5.UseVisualStyleBackColor = false;
            this.rbSi5.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // rbNo5
            // 
            this.rbNo5.AutoSize = true;
            this.rbNo5.BackColor = System.Drawing.Color.Transparent;
            this.rbNo5.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbNo5.Location = new System.Drawing.Point(53, 3);
            this.rbNo5.Name = "rbNo5";
            this.rbNo5.Size = new System.Drawing.Size(49, 23);
            this.rbNo5.TabIndex = 10062;
            this.rbNo5.TabStop = true;
            this.rbNo5.Text = "NO";
            this.rbNo5.UseVisualStyleBackColor = false;
            this.rbNo5.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblPreg5
            // 
            this.lblPreg5.BackColor = System.Drawing.Color.Transparent;
            this.lblPreg5.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreg5.Location = new System.Drawing.Point(22, 415);
            this.lblPreg5.Name = "lblPreg5";
            this.lblPreg5.Size = new System.Drawing.Size(280, 21);
            this.lblPreg5.TabIndex = 10070;
            this.lblPreg5.Text = "Sordera o enfermedades auditivas:";
            this.lblPreg5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelPreg4
            // 
            this.panelPreg4.Controls.Add(this.rbSi4);
            this.panelPreg4.Controls.Add(this.rbNo4);
            this.panelPreg4.Location = new System.Drawing.Point(311, 380);
            this.panelPreg4.Name = "panelPreg4";
            this.panelPreg4.Size = new System.Drawing.Size(106, 30);
            this.panelPreg4.TabIndex = 10069;
            // 
            // rbSi4
            // 
            this.rbSi4.AutoSize = true;
            this.rbSi4.BackColor = System.Drawing.Color.Transparent;
            this.rbSi4.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSi4.Location = new System.Drawing.Point(6, 3);
            this.rbSi4.Name = "rbSi4";
            this.rbSi4.Size = new System.Drawing.Size(41, 23);
            this.rbSi4.TabIndex = 10061;
            this.rbSi4.TabStop = true;
            this.rbSi4.Text = "SI";
            this.rbSi4.UseVisualStyleBackColor = false;
            this.rbSi4.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // rbNo4
            // 
            this.rbNo4.AutoSize = true;
            this.rbNo4.BackColor = System.Drawing.Color.Transparent;
            this.rbNo4.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbNo4.Location = new System.Drawing.Point(53, 3);
            this.rbNo4.Name = "rbNo4";
            this.rbNo4.Size = new System.Drawing.Size(49, 23);
            this.rbNo4.TabIndex = 10062;
            this.rbNo4.TabStop = true;
            this.rbNo4.Text = "NO";
            this.rbNo4.UseVisualStyleBackColor = false;
            this.rbNo4.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblPreg4
            // 
            this.lblPreg4.BackColor = System.Drawing.Color.Transparent;
            this.lblPreg4.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreg4.Location = new System.Drawing.Point(22, 383);
            this.lblPreg4.Name = "lblPreg4";
            this.lblPreg4.Size = new System.Drawing.Size(280, 21);
            this.lblPreg4.TabIndex = 10068;
            this.lblPreg4.Text = "Hipertensión Arterial:";
            this.lblPreg4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelPreg3
            // 
            this.panelPreg3.Controls.Add(this.rbSi3);
            this.panelPreg3.Controls.Add(this.rbNo3);
            this.panelPreg3.Location = new System.Drawing.Point(311, 348);
            this.panelPreg3.Name = "panelPreg3";
            this.panelPreg3.Size = new System.Drawing.Size(106, 30);
            this.panelPreg3.TabIndex = 10067;
            // 
            // rbSi3
            // 
            this.rbSi3.AutoSize = true;
            this.rbSi3.BackColor = System.Drawing.Color.Transparent;
            this.rbSi3.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSi3.Location = new System.Drawing.Point(6, 3);
            this.rbSi3.Name = "rbSi3";
            this.rbSi3.Size = new System.Drawing.Size(41, 23);
            this.rbSi3.TabIndex = 10061;
            this.rbSi3.TabStop = true;
            this.rbSi3.Text = "SI";
            this.rbSi3.UseVisualStyleBackColor = false;
            this.rbSi3.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // rbNo3
            // 
            this.rbNo3.AutoSize = true;
            this.rbNo3.BackColor = System.Drawing.Color.Transparent;
            this.rbNo3.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbNo3.Location = new System.Drawing.Point(53, 3);
            this.rbNo3.Name = "rbNo3";
            this.rbNo3.Size = new System.Drawing.Size(49, 23);
            this.rbNo3.TabIndex = 10062;
            this.rbNo3.TabStop = true;
            this.rbNo3.Text = "NO";
            this.rbNo3.UseVisualStyleBackColor = false;
            this.rbNo3.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblPreg3
            // 
            this.lblPreg3.BackColor = System.Drawing.Color.Transparent;
            this.lblPreg3.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreg3.Location = new System.Drawing.Point(22, 351);
            this.lblPreg3.Name = "lblPreg3";
            this.lblPreg3.Size = new System.Drawing.Size(280, 21);
            this.lblPreg3.TabIndex = 10066;
            this.lblPreg3.Text = "Enfermedades del Corazón:";
            this.lblPreg3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelPreg2
            // 
            this.panelPreg2.Controls.Add(this.rbSi2);
            this.panelPreg2.Controls.Add(this.rbNo2);
            this.panelPreg2.Location = new System.Drawing.Point(311, 316);
            this.panelPreg2.Name = "panelPreg2";
            this.panelPreg2.Size = new System.Drawing.Size(106, 30);
            this.panelPreg2.TabIndex = 10065;
            // 
            // rbSi2
            // 
            this.rbSi2.AutoSize = true;
            this.rbSi2.BackColor = System.Drawing.Color.Transparent;
            this.rbSi2.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSi2.Location = new System.Drawing.Point(6, 3);
            this.rbSi2.Name = "rbSi2";
            this.rbSi2.Size = new System.Drawing.Size(41, 23);
            this.rbSi2.TabIndex = 10061;
            this.rbSi2.TabStop = true;
            this.rbSi2.Text = "SI";
            this.rbSi2.UseVisualStyleBackColor = false;
            this.rbSi2.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // rbNo2
            // 
            this.rbNo2.AutoSize = true;
            this.rbNo2.BackColor = System.Drawing.Color.Transparent;
            this.rbNo2.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbNo2.Location = new System.Drawing.Point(53, 3);
            this.rbNo2.Name = "rbNo2";
            this.rbNo2.Size = new System.Drawing.Size(49, 23);
            this.rbNo2.TabIndex = 10062;
            this.rbNo2.TabStop = true;
            this.rbNo2.Text = "NO";
            this.rbNo2.UseVisualStyleBackColor = false;
            this.rbNo2.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblPreg2
            // 
            this.lblPreg2.BackColor = System.Drawing.Color.Transparent;
            this.lblPreg2.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreg2.Location = new System.Drawing.Point(22, 319);
            this.lblPreg2.Name = "lblPreg2";
            this.lblPreg2.Size = new System.Drawing.Size(280, 21);
            this.lblPreg2.TabIndex = 10064;
            this.lblPreg2.Text = "Enfermedades Pulmonares:";
            this.lblPreg2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelPreg1
            // 
            this.panelPreg1.Controls.Add(this.rbSi1);
            this.panelPreg1.Controls.Add(this.rbNo1);
            this.panelPreg1.Location = new System.Drawing.Point(311, 284);
            this.panelPreg1.Name = "panelPreg1";
            this.panelPreg1.Size = new System.Drawing.Size(106, 30);
            this.panelPreg1.TabIndex = 10063;
            // 
            // rbSi1
            // 
            this.rbSi1.AutoSize = true;
            this.rbSi1.BackColor = System.Drawing.Color.Transparent;
            this.rbSi1.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSi1.Location = new System.Drawing.Point(6, 3);
            this.rbSi1.Name = "rbSi1";
            this.rbSi1.Size = new System.Drawing.Size(41, 23);
            this.rbSi1.TabIndex = 10061;
            this.rbSi1.TabStop = true;
            this.rbSi1.Text = "SI";
            this.rbSi1.UseVisualStyleBackColor = false;
            this.rbSi1.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // rbNo1
            // 
            this.rbNo1.AutoSize = true;
            this.rbNo1.BackColor = System.Drawing.Color.Transparent;
            this.rbNo1.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbNo1.Location = new System.Drawing.Point(53, 3);
            this.rbNo1.Name = "rbNo1";
            this.rbNo1.Size = new System.Drawing.Size(49, 23);
            this.rbNo1.TabIndex = 10062;
            this.rbNo1.TabStop = true;
            this.rbNo1.Text = "NO";
            this.rbNo1.UseVisualStyleBackColor = false;
            this.rbNo1.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblPreg1
            // 
            this.lblPreg1.BackColor = System.Drawing.Color.Transparent;
            this.lblPreg1.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreg1.Location = new System.Drawing.Point(31, 287);
            this.lblPreg1.Name = "lblPreg1";
            this.lblPreg1.Size = new System.Drawing.Size(280, 21);
            this.lblPreg1.TabIndex = 10060;
            this.lblPreg1.Text = "Enfermedades Metabólicas:";
            this.lblPreg1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblAntecedentes
            // 
            this.lblAntecedentes.AutoSize = true;
            this.lblAntecedentes.BackColor = System.Drawing.Color.Transparent;
            this.lblAntecedentes.Font = new System.Drawing.Font("Roboto Condensed Light", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAntecedentes.ForeColor = System.Drawing.Color.White;
            this.lblAntecedentes.Location = new System.Drawing.Point(153, 214);
            this.lblAntecedentes.Name = "lblAntecedentes";
            this.lblAntecedentes.Size = new System.Drawing.Size(720, 53);
            this.lblAntecedentes.TabIndex = 10059;
            this.lblAntecedentes.Text = "Entrevista: Antecedentes Generales y Médicos";
            // 
            // cmbFactor
            // 
            this.cmbFactor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFactor.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbFactor.FormattingEnabled = true;
            this.cmbFactor.Items.AddRange(new object[] {
            "+",
            "-"});
            this.cmbFactor.Location = new System.Drawing.Point(825, 133);
            this.cmbFactor.Name = "cmbFactor";
            this.cmbFactor.Size = new System.Drawing.Size(56, 27);
            this.cmbFactor.TabIndex = 10058;
            this.cmbFactor.SelectedIndexChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblFactor
            // 
            this.lblFactor.BackColor = System.Drawing.Color.Transparent;
            this.lblFactor.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFactor.Location = new System.Drawing.Point(756, 136);
            this.lblFactor.Name = "lblFactor";
            this.lblFactor.Size = new System.Drawing.Size(125, 21);
            this.lblFactor.TabIndex = 10057;
            this.lblFactor.Text = "Factor Rh:";
            // 
            // cmbGrupo
            // 
            this.cmbGrupo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGrupo.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbGrupo.FormattingEnabled = true;
            this.cmbGrupo.Items.AddRange(new object[] {
            "0",
            "A",
            "B",
            "AB"});
            this.cmbGrupo.Location = new System.Drawing.Point(680, 133);
            this.cmbGrupo.Name = "cmbGrupo";
            this.cmbGrupo.Size = new System.Drawing.Size(56, 27);
            this.cmbGrupo.TabIndex = 10056;
            this.cmbGrupo.SelectedIndexChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblGrupo
            // 
            this.lblGrupo.BackColor = System.Drawing.Color.Transparent;
            this.lblGrupo.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrupo.Location = new System.Drawing.Point(529, 139);
            this.lblGrupo.Name = "lblGrupo";
            this.lblGrupo.Size = new System.Drawing.Size(145, 21);
            this.lblGrupo.TabIndex = 10055;
            this.lblGrupo.Text = "Grupo Sanguíneo:";
            this.lblGrupo.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblCm
            // 
            this.lblCm.BackColor = System.Drawing.Color.Transparent;
            this.lblCm.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCm.Location = new System.Drawing.Point(762, 87);
            this.lblCm.Name = "lblCm";
            this.lblCm.Size = new System.Drawing.Size(42, 21);
            this.lblCm.TabIndex = 10054;
            this.lblCm.Text = "cm.";
            // 
            // udTalla
            // 
            this.udTalla.BackColor = System.Drawing.Color.LightGray;
            this.udTalla.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.udTalla.Location = new System.Drawing.Point(680, 100);
            this.udTalla.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.udTalla.Name = "udTalla";
            this.udTalla.Size = new System.Drawing.Size(70, 27);
            this.udTalla.TabIndex = 10053;
            this.udTalla.ValueChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblTalla
            // 
            this.lblTalla.BackColor = System.Drawing.Color.Transparent;
            this.lblTalla.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTalla.Location = new System.Drawing.Point(529, 106);
            this.lblTalla.Name = "lblTalla";
            this.lblTalla.Size = new System.Drawing.Size(145, 21);
            this.lblTalla.TabIndex = 10052;
            this.lblTalla.Text = "Talla:";
            this.lblTalla.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblKg
            // 
            this.lblKg.BackColor = System.Drawing.Color.Transparent;
            this.lblKg.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKg.Location = new System.Drawing.Point(762, 68);
            this.lblKg.Name = "lblKg";
            this.lblKg.Size = new System.Drawing.Size(42, 21);
            this.lblKg.TabIndex = 10051;
            this.lblKg.Text = "kg.";
            // 
            // udPeso
            // 
            this.udPeso.BackColor = System.Drawing.Color.LightGray;
            this.udPeso.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.udPeso.Location = new System.Drawing.Point(680, 67);
            this.udPeso.Maximum = new decimal(new int[] {
            400,
            0,
            0,
            0});
            this.udPeso.Name = "udPeso";
            this.udPeso.Size = new System.Drawing.Size(70, 27);
            this.udPeso.TabIndex = 10050;
            this.udPeso.ValueChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblPeso
            // 
            this.lblPeso.BackColor = System.Drawing.Color.Transparent;
            this.lblPeso.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPeso.Location = new System.Drawing.Point(529, 66);
            this.lblPeso.Name = "lblPeso";
            this.lblPeso.Size = new System.Drawing.Size(145, 21);
            this.lblPeso.TabIndex = 10049;
            this.lblPeso.Text = "Peso:";
            this.lblPeso.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tbOcupacion
            // 
            this.tbOcupacion.BackColor = System.Drawing.Color.LightGray;
            this.tbOcupacion.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbOcupacion.Font = new System.Drawing.Font("Roboto", 12F);
            this.tbOcupacion.Location = new System.Drawing.Point(680, 41);
            this.tbOcupacion.Name = "tbOcupacion";
            this.tbOcupacion.Size = new System.Drawing.Size(311, 20);
            this.tbOcupacion.TabIndex = 10048;
            this.tbOcupacion.TextChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblOcupacion
            // 
            this.lblOcupacion.BackColor = System.Drawing.Color.Transparent;
            this.lblOcupacion.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOcupacion.Location = new System.Drawing.Point(529, 38);
            this.lblOcupacion.Name = "lblOcupacion";
            this.lblOcupacion.Size = new System.Drawing.Size(145, 21);
            this.lblOcupacion.TabIndex = 10047;
            this.lblOcupacion.Text = "Ocupación:";
            this.lblOcupacion.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tbDirección
            // 
            this.tbDirección.BackColor = System.Drawing.Color.LightGray;
            this.tbDirección.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbDirección.Font = new System.Drawing.Font("Roboto", 12F);
            this.tbDirección.Location = new System.Drawing.Point(680, 16);
            this.tbDirección.Name = "tbDirección";
            this.tbDirección.Size = new System.Drawing.Size(311, 20);
            this.tbDirección.TabIndex = 10046;
            this.tbDirección.TextChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblDireccion
            // 
            this.lblDireccion.BackColor = System.Drawing.Color.Transparent;
            this.lblDireccion.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDireccion.Location = new System.Drawing.Point(595, 14);
            this.lblDireccion.Name = "lblDireccion";
            this.lblDireccion.Size = new System.Drawing.Size(79, 21);
            this.lblDireccion.TabIndex = 10045;
            this.lblDireccion.Text = "Dirección:";
            this.lblDireccion.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // rbSexoX
            // 
            this.rbSexoX.AutoSize = true;
            this.rbSexoX.BackColor = System.Drawing.Color.Transparent;
            this.rbSexoX.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSexoX.Location = new System.Drawing.Point(366, 188);
            this.rbSexoX.Name = "rbSexoX";
            this.rbSexoX.Size = new System.Drawing.Size(37, 23);
            this.rbSexoX.TabIndex = 10044;
            this.rbSexoX.TabStop = true;
            this.rbSexoX.Text = "X";
            this.rbSexoX.UseVisualStyleBackColor = false;
            this.rbSexoX.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // rbSexoFem
            // 
            this.rbSexoFem.AutoSize = true;
            this.rbSexoFem.BackColor = System.Drawing.Color.Transparent;
            this.rbSexoFem.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSexoFem.Location = new System.Drawing.Point(268, 188);
            this.rbSexoFem.Name = "rbSexoFem";
            this.rbSexoFem.Size = new System.Drawing.Size(97, 23);
            this.rbSexoFem.TabIndex = 10043;
            this.rbSexoFem.TabStop = true;
            this.rbSexoFem.Text = "Femenino";
            this.rbSexoFem.UseVisualStyleBackColor = false;
            this.rbSexoFem.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblEdad
            // 
            this.lblEdad.BackColor = System.Drawing.Color.Transparent;
            this.lblEdad.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEdad.Location = new System.Drawing.Point(523, 159);
            this.lblEdad.Name = "lblEdad";
            this.lblEdad.Size = new System.Drawing.Size(145, 21);
            this.lblEdad.TabIndex = 10042;
            this.lblEdad.Text = "0";
            // 
            // rbSexoMasc
            // 
            this.rbSexoMasc.AutoSize = true;
            this.rbSexoMasc.BackColor = System.Drawing.Color.Transparent;
            this.rbSexoMasc.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSexoMasc.Location = new System.Drawing.Point(164, 188);
            this.rbSexoMasc.Name = "rbSexoMasc";
            this.rbSexoMasc.Size = new System.Drawing.Size(101, 23);
            this.rbSexoMasc.TabIndex = 10040;
            this.rbSexoMasc.TabStop = true;
            this.rbSexoMasc.Text = "Masculino";
            this.rbSexoMasc.UseVisualStyleBackColor = false;
            this.rbSexoMasc.CheckedChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // dtpFechaNac
            // 
            this.dtpFechaNac.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFechaNac.Location = new System.Drawing.Point(162, 155);
            this.dtpFechaNac.Name = "dtpFechaNac";
            this.dtpFechaNac.Size = new System.Drawing.Size(311, 27);
            this.dtpFechaNac.TabIndex = 10039;
            this.dtpFechaNac.ValueChanged += new System.EventHandler(this.dtpFechaNac_ValueChanged);
            // 
            // dtpFechaEmision
            // 
            this.dtpFechaEmision.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFechaEmision.Location = new System.Drawing.Point(162, 122);
            this.dtpFechaEmision.Name = "dtpFechaEmision";
            this.dtpFechaEmision.Size = new System.Drawing.Size(311, 27);
            this.dtpFechaEmision.TabIndex = 10038;
            this.dtpFechaEmision.ValueChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblSexo
            // 
            this.lblSexo.BackColor = System.Drawing.Color.Transparent;
            this.lblSexo.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSexo.Location = new System.Drawing.Point(11, 189);
            this.lblSexo.Name = "lblSexo";
            this.lblSexo.Size = new System.Drawing.Size(145, 21);
            this.lblSexo.TabIndex = 10037;
            this.lblSexo.Text = "Sexo:";
            this.lblSexo.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblFechaNac
            // 
            this.lblFechaNac.BackColor = System.Drawing.Color.Transparent;
            this.lblFechaNac.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechaNac.Location = new System.Drawing.Point(9, 161);
            this.lblFechaNac.Name = "lblFechaNac";
            this.lblFechaNac.Size = new System.Drawing.Size(145, 21);
            this.lblFechaNac.TabIndex = 10036;
            this.lblFechaNac.Text = "Fecha de nacimiento:";
            this.lblFechaNac.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblFechaEmision
            // 
            this.lblFechaEmision.BackColor = System.Drawing.Color.Transparent;
            this.lblFechaEmision.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechaEmision.Location = new System.Drawing.Point(11, 125);
            this.lblFechaEmision.Name = "lblFechaEmision";
            this.lblFechaEmision.Size = new System.Drawing.Size(145, 21);
            this.lblFechaEmision.TabIndex = 10035;
            this.lblFechaEmision.Text = "Fecha de Emisión:";
            this.lblFechaEmision.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tbNoDoc
            // 
            this.tbNoDoc.BackColor = System.Drawing.Color.LightGray;
            this.tbNoDoc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbNoDoc.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNoDoc.Location = new System.Drawing.Point(331, 98);
            this.tbNoDoc.Name = "tbNoDoc";
            this.tbNoDoc.Size = new System.Drawing.Size(106, 20);
            this.tbNoDoc.TabIndex = 10017;
            this.tbNoDoc.TextChanged += new System.EventHandler(this.Cambioendatos);
            this.tbNoDoc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbNoDoc_KeyPress);
            // 
            // cmbTipoDoc
            // 
            this.cmbTipoDoc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipoDoc.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTipoDoc.FormattingEnabled = true;
            this.cmbTipoDoc.Items.AddRange(new object[] {
            "DNI",
            "LC",
            "LE",
            "CI",
            "PA",
            "PE",
            "PF"});
            this.cmbTipoDoc.Location = new System.Drawing.Point(162, 92);
            this.cmbTipoDoc.Name = "cmbTipoDoc";
            this.cmbTipoDoc.Size = new System.Drawing.Size(89, 27);
            this.cmbTipoDoc.TabIndex = 10016;
            this.cmbTipoDoc.SelectedIndexChanged += new System.EventHandler(this.Cambioendatos);
            // 
            // lblNoDoc
            // 
            this.lblNoDoc.BackColor = System.Drawing.Color.Transparent;
            this.lblNoDoc.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoDoc.Location = new System.Drawing.Point(264, 98);
            this.lblNoDoc.Name = "lblNoDoc";
            this.lblNoDoc.Size = new System.Drawing.Size(66, 21);
            this.lblNoDoc.TabIndex = 10015;
            this.lblNoDoc.Text = "N° doc.:";
            this.lblNoDoc.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblTipoDoc
            // 
            this.lblTipoDoc.BackColor = System.Drawing.Color.Transparent;
            this.lblTipoDoc.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoDoc.Location = new System.Drawing.Point(11, 93);
            this.lblTipoDoc.Name = "lblTipoDoc";
            this.lblTipoDoc.Size = new System.Drawing.Size(145, 21);
            this.lblTipoDoc.TabIndex = 10014;
            this.lblTipoDoc.Text = "Tipo de Documento:";
            this.lblTipoDoc.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tbNombre
            // 
            this.tbNombre.BackColor = System.Drawing.Color.LightGray;
            this.tbNombre.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbNombre.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNombre.Location = new System.Drawing.Point(162, 40);
            this.tbNombre.Name = "tbNombre";
            this.tbNombre.Size = new System.Drawing.Size(311, 20);
            this.tbNombre.TabIndex = 10013;
            this.tbNombre.TextChanged += new System.EventHandler(this.tbApellido_TextChanged);
            // 
            // lblNombre
            // 
            this.lblNombre.BackColor = System.Drawing.Color.Transparent;
            this.lblNombre.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.Location = new System.Drawing.Point(11, 40);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(145, 21);
            this.lblNombre.TabIndex = 10012;
            this.lblNombre.Text = "Nombre:";
            this.lblNombre.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tbApellido
            // 
            this.tbApellido.BackColor = System.Drawing.Color.LightGray;
            this.tbApellido.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbApellido.Font = new System.Drawing.Font("Roboto", 12F);
            this.tbApellido.Location = new System.Drawing.Point(162, 14);
            this.tbApellido.Name = "tbApellido";
            this.tbApellido.Size = new System.Drawing.Size(311, 20);
            this.tbApellido.TabIndex = 10011;
            this.tbApellido.TextChanged += new System.EventHandler(this.tbApellido_TextChanged);
            // 
            // lblApellido
            // 
            this.lblApellido.BackColor = System.Drawing.Color.Transparent;
            this.lblApellido.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApellido.Location = new System.Drawing.Point(11, 12);
            this.lblApellido.Name = "lblApellido";
            this.lblApellido.Size = new System.Drawing.Size(145, 21);
            this.lblApellido.TabIndex = 10010;
            this.lblApellido.Text = "Apellido:";
            this.lblApellido.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblTitEdad
            // 
            this.lblTitEdad.BackColor = System.Drawing.Color.Transparent;
            this.lblTitEdad.Font = new System.Drawing.Font("Roboto Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitEdad.Location = new System.Drawing.Point(382, 159);
            this.lblTitEdad.Name = "lblTitEdad";
            this.lblTitEdad.Size = new System.Drawing.Size(145, 21);
            this.lblTitEdad.TabIndex = 10041;
            this.lblTitEdad.Text = "Edad:";
            this.lblTitEdad.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // printDocument1
            // 
            this.printDocument1.DocumentName = "Declaración Jurada";
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // pbEscudo
            // 
            this.pbEscudo.Image = global::rocadeclaracion.Properties.Resources.Escudo_de_General_Roca;
            this.pbEscudo.Location = new System.Drawing.Point(1106, 113);
            this.pbEscudo.Name = "pbEscudo";
            this.pbEscudo.Size = new System.Drawing.Size(106, 87);
            this.pbEscudo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbEscudo.TabIndex = 6;
            this.pbEscudo.TabStop = false;
            this.pbEscudo.Visible = false;
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(23)))), ((int)(((byte)(36)))));
            this.BackgroundImage = global::rocadeclaracion.Properties.Resources.fondoform_01;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1354, 788);
            this.ControlBox = false;
            this.Controls.Add(this.pbEscudo);
            this.Controls.Add(this.panelEntrevista);
            this.Controls.Add(this.btnLeerDNI);
            this.Controls.Add(this.msMenuPrincipal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MainMenuStrip = this.msMenuPrincipal;
            this.Name = "frmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Declaración Jurada General Roca";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmPrincipal_FormClosing);
            this.Load += new System.EventHandler(this.frmPrincipal_Load);
            this.msMenuPrincipal.ResumeLayout(false);
            this.msMenuPrincipal.PerformLayout();
            this.panelEntrevista.ResumeLayout(false);
            this.panelEntrevista.PerformLayout();
            this.panelPreg21.ResumeLayout(false);
            this.panelPreg21.PerformLayout();
            this.panelPreg20.ResumeLayout(false);
            this.panelPreg20.PerformLayout();
            this.panelPreg19.ResumeLayout(false);
            this.panelPreg19.PerformLayout();
            this.panelPreg18.ResumeLayout(false);
            this.panelPreg18.PerformLayout();
            this.panelPreg17.ResumeLayout(false);
            this.panelPreg17.PerformLayout();
            this.panelPreg16.ResumeLayout(false);
            this.panelPreg16.PerformLayout();
            this.panelPreg15.ResumeLayout(false);
            this.panelPreg15.PerformLayout();
            this.panelPreg14.ResumeLayout(false);
            this.panelPreg14.PerformLayout();
            this.panelPreg13.ResumeLayout(false);
            this.panelPreg13.PerformLayout();
            this.panelPreg12.ResumeLayout(false);
            this.panelPreg12.PerformLayout();
            this.panelPreg11.ResumeLayout(false);
            this.panelPreg11.PerformLayout();
            this.panelPreg10.ResumeLayout(false);
            this.panelPreg10.PerformLayout();
            this.panelPreg9.ResumeLayout(false);
            this.panelPreg9.PerformLayout();
            this.panelPreg8.ResumeLayout(false);
            this.panelPreg8.PerformLayout();
            this.panelPreg7.ResumeLayout(false);
            this.panelPreg7.PerformLayout();
            this.panelPreg6.ResumeLayout(false);
            this.panelPreg6.PerformLayout();
            this.panelPreg5.ResumeLayout(false);
            this.panelPreg5.PerformLayout();
            this.panelPreg4.ResumeLayout(false);
            this.panelPreg4.PerformLayout();
            this.panelPreg3.ResumeLayout(false);
            this.panelPreg3.PerformLayout();
            this.panelPreg2.ResumeLayout(false);
            this.panelPreg2.PerformLayout();
            this.panelPreg1.ResumeLayout(false);
            this.panelPreg1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udTalla)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udPeso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEscudo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip msMenuPrincipal;
        private System.Windows.Forms.ToolStripMenuItem principalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemNuevoExam;
        private System.Windows.Forms.ToolStripMenuItem exportarPDFToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemImprimir;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemSalir;
        private System.Windows.Forms.ToolStripMenuItem herramientasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configuraciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imprimirtoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportarpdftoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevotoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem separaciontoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nombretoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tituloVentanaToolStripMenuItem;
        private System.Windows.Forms.Button btnLeerDNI;
        private System.Windows.Forms.Panel panelEntrevista;
        private System.Windows.Forms.Label lblCm;
        private System.Windows.Forms.NumericUpDown udTalla;
        private System.Windows.Forms.Label lblTalla;
        private System.Windows.Forms.Label lblKg;
        private System.Windows.Forms.NumericUpDown udPeso;
        private System.Windows.Forms.Label lblPeso;
        private System.Windows.Forms.TextBox tbOcupacion;
        private System.Windows.Forms.Label lblOcupacion;
        private System.Windows.Forms.TextBox tbDirección;
        private System.Windows.Forms.Label lblDireccion;
        private System.Windows.Forms.RadioButton rbSexoX;
        private System.Windows.Forms.RadioButton rbSexoFem;
        private System.Windows.Forms.Label lblEdad;
        private System.Windows.Forms.RadioButton rbSexoMasc;
        private System.Windows.Forms.DateTimePicker dtpFechaNac;
        private System.Windows.Forms.DateTimePicker dtpFechaEmision;
        private System.Windows.Forms.Label lblSexo;
        private System.Windows.Forms.Label lblFechaNac;
        private System.Windows.Forms.Label lblFechaEmision;
        private System.Windows.Forms.TextBox tbNoDoc;
        private System.Windows.Forms.ComboBox cmbTipoDoc;
        private System.Windows.Forms.Label lblNoDoc;
        private System.Windows.Forms.Label lblTipoDoc;
        private System.Windows.Forms.TextBox tbNombre;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.TextBox tbApellido;
        private System.Windows.Forms.Label lblApellido;
        private System.Windows.Forms.Label lblTitEdad;
        private System.Windows.Forms.RadioButton rbNo1;
        private System.Windows.Forms.RadioButton rbSi1;
        private System.Windows.Forms.Label lblPreg1;
        private System.Windows.Forms.Label lblAntecedentes;
        private System.Windows.Forms.ComboBox cmbFactor;
        private System.Windows.Forms.Label lblFactor;
        private System.Windows.Forms.ComboBox cmbGrupo;
        private System.Windows.Forms.Label lblGrupo;
        private System.Windows.Forms.Panel panelPreg1;
        private System.Windows.Forms.Panel panelPreg2;
        private System.Windows.Forms.RadioButton rbSi2;
        private System.Windows.Forms.RadioButton rbNo2;
        private System.Windows.Forms.Label lblPreg2;
        private System.Windows.Forms.Panel panelPreg3;
        private System.Windows.Forms.RadioButton rbSi3;
        private System.Windows.Forms.RadioButton rbNo3;
        private System.Windows.Forms.Label lblPreg3;
        private System.Windows.Forms.Panel panelPreg11;
        private System.Windows.Forms.RadioButton rbSi11;
        private System.Windows.Forms.RadioButton rbNo11;
        private System.Windows.Forms.Label lblPreg11;
        private System.Windows.Forms.Panel panelPreg10;
        private System.Windows.Forms.RadioButton rbSi10;
        private System.Windows.Forms.RadioButton rbNo10;
        private System.Windows.Forms.Label lblPreg10;
        private System.Windows.Forms.Panel panelPreg9;
        private System.Windows.Forms.RadioButton rbSi9;
        private System.Windows.Forms.RadioButton rbNo9;
        private System.Windows.Forms.Label lblPreg9;
        private System.Windows.Forms.Panel panelPreg8;
        private System.Windows.Forms.RadioButton rbSi8;
        private System.Windows.Forms.RadioButton rbNo8;
        private System.Windows.Forms.Label lblPreg8;
        private System.Windows.Forms.Panel panelPreg7;
        private System.Windows.Forms.RadioButton rbSi7;
        private System.Windows.Forms.RadioButton rbNo7;
        private System.Windows.Forms.Label lblPreg7;
        private System.Windows.Forms.Panel panelPreg6;
        private System.Windows.Forms.RadioButton rbSi6;
        private System.Windows.Forms.RadioButton rbNo6;
        private System.Windows.Forms.Label lblPreg6;
        private System.Windows.Forms.Panel panelPreg5;
        private System.Windows.Forms.RadioButton rbSi5;
        private System.Windows.Forms.RadioButton rbNo5;
        private System.Windows.Forms.Label lblPreg5;
        private System.Windows.Forms.Panel panelPreg4;
        private System.Windows.Forms.RadioButton rbSi4;
        private System.Windows.Forms.RadioButton rbNo4;
        private System.Windows.Forms.Label lblPreg4;
        private System.Windows.Forms.TextBox tbNacion;
        private System.Windows.Forms.Label lblNacion;
        private System.Windows.Forms.Panel panelPreg15;
        private System.Windows.Forms.RadioButton rbSi15;
        private System.Windows.Forms.RadioButton rbNo15;
        private System.Windows.Forms.Label lblPreg15;
        private System.Windows.Forms.Panel panelPreg14;
        private System.Windows.Forms.RadioButton rbSi14;
        private System.Windows.Forms.RadioButton rbNo14;
        private System.Windows.Forms.Label lblPreg14;
        private System.Windows.Forms.Panel panelPreg13;
        private System.Windows.Forms.RadioButton rbSi13;
        private System.Windows.Forms.RadioButton rbNo13;
        private System.Windows.Forms.Label lblPreg13;
        private System.Windows.Forms.Panel panelPreg12;
        private System.Windows.Forms.RadioButton rbSi12;
        private System.Windows.Forms.RadioButton rbNo12;
        private System.Windows.Forms.Label lblPreg12;
        private System.Windows.Forms.Panel panelPreg19;
        private System.Windows.Forms.RadioButton rbSi19;
        private System.Windows.Forms.RadioButton rbNo19;
        private System.Windows.Forms.Label lblPreg19;
        private System.Windows.Forms.Panel panelPreg18;
        private System.Windows.Forms.RadioButton rbSi18;
        private System.Windows.Forms.RadioButton rbNo18;
        private System.Windows.Forms.Label lblPreg18;
        private System.Windows.Forms.Panel panelPreg17;
        private System.Windows.Forms.RadioButton rbSi17;
        private System.Windows.Forms.RadioButton rbNo17;
        private System.Windows.Forms.Label lblPreg17;
        private System.Windows.Forms.Panel panelPreg16;
        private System.Windows.Forms.RadioButton rbSi16;
        private System.Windows.Forms.RadioButton rbNo16;
        private System.Windows.Forms.Label lblPreg16;
        private System.Windows.Forms.Panel panelPreg21;
        private System.Windows.Forms.RadioButton rbSi21;
        private System.Windows.Forms.RadioButton rbNo21;
        private System.Windows.Forms.Label lblPreg21;
        private System.Windows.Forms.Panel panelPreg20;
        private System.Windows.Forms.RadioButton rbSi20;
        private System.Windows.Forms.RadioButton rbNo20;
        private System.Windows.Forms.Label lblPreg20;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.PictureBox pbEscudo;
    }
}

