﻿namespace rocadeclaracion
{
    partial class frmNoGuardado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnNoDescarta = new System.Windows.Forms.Button();
            this.btnDescarta = new System.Windows.Forms.Button();
            this.lblMsgNoGuardado = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnNoDescarta
            // 
            this.btnNoDescarta.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            this.btnNoDescarta.FlatAppearance.BorderSize = 0;
            this.btnNoDescarta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNoDescarta.ForeColor = System.Drawing.Color.White;
            this.btnNoDescarta.Location = new System.Drawing.Point(224, 92);
            this.btnNoDescarta.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnNoDescarta.Name = "btnNoDescarta";
            this.btnNoDescarta.Size = new System.Drawing.Size(112, 34);
            this.btnNoDescarta.TabIndex = 6;
            this.btnNoDescarta.Text = "No";
            this.btnNoDescarta.UseVisualStyleBackColor = false;
            this.btnNoDescarta.Click += new System.EventHandler(this.btnNoDescarta_Click);
            // 
            // btnDescarta
            // 
            this.btnDescarta.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            this.btnDescarta.FlatAppearance.BorderSize = 0;
            this.btnDescarta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDescarta.ForeColor = System.Drawing.Color.White;
            this.btnDescarta.Location = new System.Drawing.Point(22, 90);
            this.btnDescarta.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnDescarta.Name = "btnDescarta";
            this.btnDescarta.Size = new System.Drawing.Size(112, 34);
            this.btnDescarta.TabIndex = 5;
            this.btnDescarta.Text = "Si";
            this.btnDescarta.UseVisualStyleBackColor = false;
            this.btnDescarta.Click += new System.EventHandler(this.btnDescarta_Click);
            // 
            // lblMsgNoGuardado
            // 
            this.lblMsgNoGuardado.BackColor = System.Drawing.Color.Transparent;
            this.lblMsgNoGuardado.Location = new System.Drawing.Point(18, 14);
            this.lblMsgNoGuardado.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMsgNoGuardado.Name = "lblMsgNoGuardado";
            this.lblMsgNoGuardado.Size = new System.Drawing.Size(318, 73);
            this.lblMsgNoGuardado.TabIndex = 4;
            this.lblMsgNoGuardado.Text = "La declaración jurada no ha sido exportada ni impresa. Desea descartar los cambio" +
    "s?";
            this.lblMsgNoGuardado.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // frmNoGuardado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::rocadeclaracion.Properties.Resources.fondoform_01;
            this.ClientSize = new System.Drawing.Size(361, 138);
            this.Controls.Add(this.btnNoDescarta);
            this.Controls.Add(this.btnDescarta);
            this.Controls.Add(this.lblMsgNoGuardado);
            this.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "frmNoGuardado";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Examen No Guardado";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnNoDescarta;
        private System.Windows.Forms.Button btnDescarta;
        private System.Windows.Forms.Label lblMsgNoGuardado;
    }
}